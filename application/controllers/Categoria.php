<?php
class Categoria extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Categoria_model');

    }

    public function listar() {

        $categorias = $this->Categoria_model->listar();
        echo(json_encode($categorias));

        /*
    	if($this->session->userdata('logeado')){
    		$categorias = $this->Categoria_model->listar();
    	echo(json_encode($categorias));
    	}
    	else{
    		echo(json_encode(false));
    	}*/
    	
    }

}