<?php
class Eventos extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Evento_model');
        $this->load->model('Menu_model');
        $this->load->model('Tipoticket_model');
        $this->load->model('Metodopago_model');
        $this->load->model('Client_model');
        $this->load->model('Compra_model');
        $this->load->model('Ticket_model');
        $this->load->model('Paquete_model');
        $this->load->model('Compracomida_model');
    }

	public function listar() {
		$buscar = $this->input->post("buscar");
        $numeropagina = $this->input->post("nropagina");
        $lugar = $this->input->post("lugar");
        $categoria = $this->input->post("categoria");

        if($lugar==""){$lugar = false;}
        if($categoria==""){$categoria = false;}

        $inicio = ($numeropagina -1)*20;
        $data = array(
            "eventos" => $this->Evento_model->buscar($buscar,$inicio,$lugar,$categoria),
            "totalregistros" => count($this->Evento_model->buscar($buscar))
        );
        echo json_encode($data);
	}

    public function carousel() {
        $data = array(
            "eventos" => $this->Evento_model->carousel(),
            "totalregistros" => count($this->Evento_model->carousel())
        );
        echo json_encode($data);
    }

    public function proximos() {
        $data = array(
            "eventos" => $this->Evento_model->proximoseventos(),
            "totalregistros" => count($this->Evento_model->proximoseventos())
        );
        echo json_encode($data);
    }

    public function detalle() {
        $idevento = $this->input->post("idevento");

        $detalle = $this->Evento_model->detalle($idevento);
        $menu = $this->Menu_model->menuPorEvento($idevento);
        $tickets = $this->Tipoticket_model->tipoticketsPorEvento($idevento);
        $descuentos = $this->Paquete_model->paquetesPorEvento($idevento);

        if($detalle){
            $data = array(
                "evento" => $detalle,
                "menu" => $menu,
                "tickets" => $tickets,
                "descuentos" => $descuentos
            );
            echo json_encode($data);
        }
        else{
            echo json_encode(['error' => 'No se encuentra la informacion para el evento dado']);
        }
    }

    public function comprar() {

    	$fecha = $this->input->post("fecha");
    	$metododepago = $this->input->post("metododepago");
        $idevento = $this->input->post("idevento");
        $idusuario = $this->session->userdata('idusuario');

        $erroresCompra = $this->erroresCompra($fecha,$metododepago,$idevento);
        if (count($erroresCompra) == 0) {

        	$items = $this->input->post("items");
        	$erroresItems = $this->erroresItems($fecha,$metododepago,$idevento,$items);
        	if (count($erroresItems) == 0){

                $subtotal = $this->calcularSubtotal($items,$idevento);
                if ($subtotal > 0) {

                    $saldo = $this->Client_model->saldo();
                    if ($subtotal <= $saldo) {

                        $saldo = $saldo-$subtotal;
                        if ($this->Client_model->cambiarSaldo($idusuario,$saldo)) {

                            $compra = [
                                "idusuario"    => $idusuario,
                                "importe"      => $subtotal,
                                "idmetodopago" => $metododepago,
                                "idevento"     => $idevento
                            ];
                            $resultadocompra = $this->Compra_model->insertar($compra);
                            if ($resultadocompra) {
                                    $idcompra = $resultadocompra['idcompra'];
                                    $errorinsertaritems = $this->insertarItems($items,$idevento,$idcompra,$idusuario,$fecha);
                                    if (count($errorinsertaritems) == 0) {
                                        echo json_encode(array("exito" => true,"info" => "Compra exitosa.\n Recibirá un mail con los codigos para activar las entradas o reclamar las comidas."));
                                    }else{
                                        echo json_encode(array("exito" => false,"errores" => $errorinsertaritems));
                                    }
                            }else{
                                $errores["ERROR"] = "Error al realizar la compra";
                                echo json_encode(array("exito" => false,"errores" => $errores));
                            }
                        }else{
                            $errores["ERROR"] = "Error al intentar actualizar el saldo del cliente";
                            echo json_encode(array("exito" => false,"errores" => $errores));
                        }
                    }else{
                        $errores["SALDO_INSUFICIENTE"] = "El saldo disponible no es suficiente para realizar la compra: Saldo ".$saldo." Subtotal ".$subtotal;
                        echo json_encode(array("exito" => false,"errores" => $errores));
                    }
                }else{
                    $errores["SUBTOTAL_INVALIDO"] = "El subtotal de la compra debe ser mayor a 0.";
                    echo json_encode(array("exito" => false,"errores" => $errores));
                }
        	}else{
        		echo json_encode(array("exito" => false,"errores" => $erroresItems));
        	}
        }else{
        	echo json_encode(array("exito" => false,"errores" => $erroresCompra));
        }
    }

    public function generarCodigo(){
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 50; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function sendmail($destinatario,$asuntomail,$cuerpomail,$qrs) {

        // Configure email library
        $this->load->library('mailer');
        $mail = $this->mailer->load();
        $mail->isSMTP();
        $mail->Host = '192.168.21.62';
        $mail->Username = 'tickasur@tecnisur.local';
        $mail->Password = 'Administrator1';
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Port = 465;
        $mail->SMTPOptions = array(
            'ssl' => [
                'verify_peer' => false,
                'verify_depth' => 3,
                'allow_self_signed' => true,
                'peer_name' => 'exchange.tecnisur.local'
            ],
        );

        $mail->setFrom('tickasur@tecnisur.local', 'tickasur@tecnisur.local');
        $mail->addAddress($destinatario);
        $mail->Subject = $asuntomail;
        $mail->Body = $cuerpomail;

        foreach ($qrs as $index => $qr) {
            $mail->AddAttachment($qr);
        }

        if(!$mail->send()){
            foreach ($qrs as $index => $qr) {
                unlink($qr);
            }
            return $mail->ErrorInfo;
        }else{
            foreach ($qrs as $index => $qr) {
                unlink($qr);
            }
            return true;
        }
    }

    public function erroresCompra($fecha,$metododepago,$idevento) {
    	$errores = [];
    	if(!$this->Evento_model->valido($idevento)){
    		$errores["Evento"] = "El evento ID ".$idevento." parece no estar dado de alta.";
    	}

    	if($fecha < date("Y-m-d H:i:s")){
        	$errores["Fecha"] = "La fecha elegida no puede ser menor a la fecha actual. Fecha ".$fecha."";
        }

        if(!$this->Metodopago_model->existe($metododepago)){
        	$errores["Metodo de pago"] = "El metodo de pago ID ".$metododepago." parece no estar dado de alta"; //porque no existe o porque no este con baja logica
        }
        return $errores;
    }

    public function erroresItems($fecha,$metododepago,$idevento,$items){
    	$errores  = [];
    	$tickets  = [];
    	$comidas  = [];
    	$paquetes = [];

    	foreach ($items as $clave => $arrayunidades) {
    		if ($clave == "tickets") {
    			$tickets = $arrayunidades;
    		}
    		if ($clave == "paquetes") {
    			$paquetes = $arrayunidades;
    		}
    		if ($clave == "comidas") {
    			$comidas = $arrayunidades;
    		}
    	}

    	foreach ($comidas as $comida) {
    		if(!$this->Menu_model->existe($comida['idcomida'],$idevento)){
	        	$errores["COMIDA_INVALIDA"] = "La comida ID ".$comida['idcomida']." no existe o no esta habilitada para el evento"; //No existe o esta dado de baja
	        }
    	}
        //$errores["paquetes"] = $paquetes; //
    	foreach ($paquetes as $index => $paquete) {
    		if ($this->Paquete_model->existe($paquete['idpaquete'])) {
    			//Verifico si el paquete esta dado de alta
    			$paqueteLegit = $this->Paquete_model->buscarPorId($paquete['idpaquete']);//Este es el paquete que obtenemos desde la base de datos y vamos a utilizarlo para verificar la informacion que nos envia el cliente.
    			$metodosdepago = $this->Paquete_model->metodosdepago($paquete['idpaquete'],$paquete['idevento']);
    			$metodoesvalido = false;
    			foreach ($metodosdepago as $index => $metodo) {
    				if ($metododepago == $metodo['idmetodopago']) {
    					$metodoesvalido = true;
    				}
    			}
    			if (!$metodoesvalido){
    				$errores["METODODEPAGO_INVALIDO"] = "El metodo de pago ".$metododepago." es invalido.";
    			}
    			if ($paquete['fecha'] == $paqueteLegit['fecha'] ) {
    				if ($fecha != $paqueteLegit['fecha']) {
    					$errores["FECHA_INVALIDA"] = "La fecha de la compra ".$fecha." no corresponde con la fecha del paquete en el sistema.";
    				}
    			}else{
    				$errores["FECHA_INVALIDA"] = "La fecha".$paquete['fecha']." no aparece en el sistema";
    			}
    		}else{
    			$errores["PAQUETE_INVALIDO"] = "El paquete ID ".$paquete['idpaquete']." no existe o no esta habilitado para el evento";
    		}
    		if ($this->Paquete_model->stock($paquete['idpaquete']) > 0) {
    			if ($paquete['cantidadelegida'] > $this->Paquete_model->stock($paquete['idpaquete'])) {
                    $errores["CANTIDAD_INVALIDO"] = "El pedido del paquete ID ".$paquete['idpaquete']." excede el stock presente.";
    			}
    		}else{
    			$errores["STOCK_INVALIDO"] = "El paquete ID ".$paquete['idpaquete']." no tiene mas stock.";
    		}
		}
        //$errores["tickets"] = $tickets;
        foreach ($tickets as $index => $tipoticket) {
            $idticket = $tipoticket['idtipoticket'];

            if(!$this->Tipoticket_model->existe($idticket,$idevento)){
                    $errores["TICKET_INVALIDO ".$tipoticket['idtipoticket']] = "El ticket que se desea comprar no es valido."; //No existe o esta dado de baja
            }else{
                $fechaticket = $this->Tipoticket_model->fecha($idticket);
                if( $fechaticket["fecha"] != $fecha){
                    $errores["FECHA_INVALIDA"] = "La fecha de los tickets no coincide con la fecha enviada";
                }

                $stock = $this->Tipoticket_model->stock($idticket);
                if ($stock > 0) {
                    $totalPedidas = 0;
                    if (isset($tipoticket['cantidadelegidaspaquete'])) {
                        foreach ($tipoticket['cantidadelegidaspaquete'] as $idpaquete => $cantidad) {
                            $totalPedidas += $cantidad;
                        }
                    }
                    if (isset($tipoticket['individuales'])) {
                        $totalPedidas += $tipoticket['individuales'];
                    }
                    if ($totalPedidas > $stock) {
                        $errores["CANTIDAD_INVALIDA ".$idticket] = "El ticket ID ".$idticket." excede el stock presente.";
                    }
                }else{
                    $errores["STOCK_INVALIDO"] = "El tipo de ticket ID ".$idticket." no tiene mas stock.";
                }
                $maximo = $this->Tipoticket_model->cantidad($idticket);
                $maximo = $maximo['cantidad'];
                if ($tipoticket['numerado']) {
                    $elegidas = [];
                    if (isset($tipoticket['elegidasPaquete'])) {
                        foreach ($tipoticket['elegidasPaquete'] as $idpaquete => $arrayelegidas) {
                            foreach ($arrayelegidas as $idform => $numero) {
                                if ($numero> $maximo) {
                                    $errores["NUMERO_INVALIDO ".$idticket] = "Numero maximo ".$maximo;
                                }else if ($numero<=0) {
                                    $errores["NUMERO_INVALIDO ".$idticket] = "El numero no puede ser igual o menor a 0.";
                                }else{
                                    array_push($elegidas, $numero);  
                                }    
                            }
                        }
                    }
                    if (isset($tipoticket['elegidassueltas'])) {
                        foreach ($tipoticket['elegidassueltas'] as $idform => $numero) {
                            array_push($elegidas, $numero);
                        }     
                    }
                    $numerosOcupados = $this->Ticket_model->ticketsVendidos($idticket,$tipoticket['numerado']);
                    if(count($this->asientosDuplicados($elegidas)) > 0){
                        $errores["NUMEROS_DUPLICADOS ".$idticket] = "Existen numeros elegidos que estan duplciados ".implode(",",$this->asientosDuplicados($elegidas));
                    }
                    if (count($this->ocupadasElegidas($elegidas,$numerosOcupados)) > 0){
                        $errores["NUMEROS_OCUPADOS ".$idticket] = "Existen numeros elegidos que estan ocupados ".implode(",",$this->ocupadasElegidas($elegidas,$numerosOcupados));
                    }
                }
                if (isset($tipoticket['cantidadelegidaspaquete'])) {
                    foreach ($tipoticket['cantidadelegidaspaquete'] as $idpaquete => $cantidad) {
                        if($cantidad > 0 ){
                            if (count($this->validarIntegridad($idpaquete,$idticket,$cantidad,$paquetes)) > 0){
                                $errores["INTEGRIDAD_INVALIDA ".$idpaquete] = "No se pudo verificar si es valida la cantidad de tickets para el ticket ID ".$idticket.".";
                            }
                        }
                    }
                }
            }
        }
        return $errores;
    }

    public function ocupadasElegidas($elegidas,$ocupadas){
        $return = [];
        $errores["ocupadas"] = "";

        if (isset($ocupadas['vacio'])) {
            return $return;
        }
        $arrayocupadas = [];
        foreach ($ocupadas as $index => $valor) {
            $numeroocupado = $valor['numero'];
            array_push($arrayocupadas,$numeroocupado);
        }
        foreach ($elegidas as $index => $numeroasiento) {
            foreach ($arrayocupadas as $index => $numero) {

                if($numero == $numeroasiento){
                    if (!in_array($numero, $return)) {
                        array_push($return,$numero);
                    }
                }
                
            }
        }
        //echo json_encode(array("exito" => true,"colision" => $return));
        return $return;
    }

    public function asientosDuplicados($elegidas){
        $duplicados = [];
        sort($elegidas);
        $elegidas = $elegidas;
        foreach ($elegidas as $idform => $numeroasiento) {
            foreach ($elegidas as $idform2 => $numeroasiento2) {
                if($idform > $idform2){
                    if($numeroasiento == $numeroasiento2){
                        if (!in_array($numeroasiento2, $duplicados)) {
                            array_push($duplicados,$numeroasiento2);
                        }
                    }
                }
            }
        }
        return $duplicados;
    }

    public function validarIntegridad($idpaquete,$idticket,$cantidad,$paquetes){
        $errores = [];
        // $cantidad -> Cantidad de tickets pedidas es el resultado
        $elegirpaquete = [];
        foreach ($paquetes as $index => $paquete) {
            if ($paquete['idpaquete'] == $idpaquete) {
                $elegirpaquete = $paquete;
            }
        }
        if ($elegirpaquete) {
            $cantidadPaquete = $elegirpaquete['cantidadelegida']; //la cantidad elegida de dicho paquete

            $ticketsDelPaquete = $this->Paquete_model->ticketsdelpaquete($idpaquete);//$elegirpaquete['tipotickets']; //aca hay que conseguir los tickets desde el sistema

            foreach ($ticketsDelPaquete as $key => $tipoticket) {
                foreach ($tipoticket as $key => $valor) {
                    if ($tipoticket['idtipoticket'] == $idticket) {
                        $cantidadporPaquete = $tipoticket['cantidad'];
                    }
                }
            }
            if (isset($cantidadporPaquete)) {
                if ($cantidad != ($cantidadporPaquete*$cantidadPaquete)) {
                    $errores["INTEGRIDAD_INVALIDA"] = "Error al calcular la cantidad pedida del ticket y la cantidad de paquetes pedidos.";
                    //echo json_encode(array("exito" => true,"errores" => $errores));
                }
            }else{
                $errores["PAQUETE_INVALIDO ".$idpaquete] = "Imposible adquirir la cantidad de tickets por paquete para el ticket ".$idticket;
            }
        }else{
            $errores["PAQUETE_INVALIDO ".$idpaquete] = "Error al intentar localizar el paquete para realizar el calculo de integridad";
        }
        return $errores;
    }

    public function calcularSubtotal($items,$idevento){
        $subtotal = 0;
        $tickets  = [];
        $comidas  = [];
        $paquetes = [];

        foreach ($items as $clave => $arrayunidades) {
            if ($clave == "tickets") {
                $tickets = $arrayunidades;
            }
            if ($clave == "paquetes") {
                $paquetes = $arrayunidades;
            }
            if ($clave == "comidas") {
                $comidas = $arrayunidades;
            }
        }
        foreach ($tickets as $index => $ticket) {
            $precio = $this->Tipoticket_model->precio($ticket['idtipoticket']);
            if (isset($ticket['individuales'])) {
                $subtotal += $precio['precio']*$ticket['individuales'];
            }  
        }
        foreach ($paquetes as $index => $paquete) {
            if (isset($paquete['cantidadelegida'])) {
                $precio = $this->Paquete_model->precioPorId($paquete['idpaquete']);
                $subtotal += $precio*$paquete['cantidadelegida'];
            }
        }
        foreach ($comidas as $comida) {
            $precio = $this->Menu_model->precio($comida['idcomida'],$idevento);
            $cantidad = $comida['cantidad'];
            $subtotal += $precio*$cantidad;
        }
    return ceil($subtotal);
    }

    public function insertarItems($items,$idevento,$idcompra,$idusuario,$fecha){ //QRcode($idevento,$autogenerado,$id,$tipo)
        $asuntomail = "Compra lista : ".$idcompra;
        $cuerpomail = "Compra realizada con exito, sus items son : \n";
        $cuerpomail .= "\n";
        $errores   = [];
        $tickets   = [];
        $comidas   = [];
        $paquetes  = [];
        $qrs       = [];

        foreach ($items as $clave => $arrayunidades) {
            if ($clave == "tickets") {
                $tickets = $arrayunidades;
            }
            if ($clave == "comidas") {
                $comidas = $arrayunidades;
            }
            if ($clave == "paquetes") {
                $paquetes = $arrayunidades;
            }
        } 

        foreach ($paquetes as $index => $paquete) {

            if (isset($paquete['comidas'])) {
                $comidasdelpaquete = $paquete['comidas'];
                foreach ($comidasdelpaquete as $index => $comidadelpaquete){
                    $cantidainsertar = $paquete['cantidadelegida']*$comidadelpaquete['cantidad'];
                    $idcomida = $comidadelpaquete['idcomida'];

                    for ($i=0; $i < $cantidainsertar; $i++) { 
                        $autogenerado = $this->generarCodigo();
                        $insertar = [
                            "idcomida" => $idcomida,
                            "idevento" => $idevento,
                            "idcompra" => $idcompra,
                            "autogenerado" => $autogenerado
                        ];
                        $resultadocomida = $this->Compracomida_model->insertar($insertar);
                        if($resultadocomida){
                            $cuerpomail .= "La comida numero ".$resultadocomida['identificador']. " codigo de seguridad : ". $resultadocomida['autogenerado']."\n";
                            $cuerpomail .= "\n";
                            $insertarqr = $this->QRcode($idevento,$autogenerado,$resultadocomida['identificador'],"comida");
                            array_push($qrs, $insertarqr);
                        }
                        else{
                            $errores["ERROR"] = "Error al intentar insertar la comida";   
                        }
                    }
                }
            }
            $insertar = [
                            "idpaquete" => $paquete['idpaquete'],
                            "idcompra"  => $idcompra,
                            "cantidad"  => $paquete['cantidadelegida']
                        ];
            $resultadoinsertarpaquete = $this->Paquete_model->insertarcompra($insertar);
            if(!$resultadoinsertarpaquete){
                $errores["ERROR"] = "Error al insertar el paquete";
            }

        }
        
        foreach ($tickets as $index => $ticket) {

            $idticket = $ticket['idtipoticket'];
            $numerado = $ticket['numerado'];
            $totalPedidas = 0;
            if (isset($ticket['cantidadelegidaspaquete'])) {
                foreach ($ticket['cantidadelegidaspaquete'] as $idpaquete => $cantidad) {
                    $totalPedidas += $cantidad;
                }
            }
            if (isset($ticket['individuales'])) {
                $totalPedidas += $ticket['individuales'];
            }

            if($numerado){
                $elegidas = [];
                if (isset($ticket['elegidasPaquete'])) {
                    foreach ($ticket['elegidasPaquete'] as $idpaquete => $arrayelegidas) {
                        foreach ($arrayelegidas as $idform => $numero) {
                            array_push($elegidas, $numero);
                        }
                    }
                }
                if (isset($ticket['elegidassueltas'])) {
                    foreach ($ticket['elegidassueltas'] as $idform => $numero) {
                        array_push($elegidas, $numero);
                    }     
                }
            }

            for ($i=0; $i < $totalPedidas ; $i++) {
                $autogenerado = $this->generarCodigo();

                if($numerado == 1){
                    if(isset($numero)){
                        $numero = $elegidas[$i];    
                    }else{
                        $errores["ERROR"] = "Error no se encuentra el numero de los asientos numerados elegidos";   
                        return $errores;
                    }                    
                }else{
                    $numero = $this->Ticket_model->ultimoNumero($idticket)+1;
                }

                $insertar = [
                    "numero" => $numero,
                    "idtipoticket" => $idticket,
                    "idcompra" => $idcompra,
                    "autogenerado" => $autogenerado
                ];
                $resultadoticket = $this->Ticket_model->insertar($insertar);
                if(!$resultadoticket){
                    $errores["ERROR"] = "Error al intentar insertar el ticket ".$idticket." con el numero ".$numero;   
                }else{
                    $cuerpomail .= "El ticket numero ".$resultadoticket['idticket']. " codigo de seguridad : ". $resultadoticket['autogenerado']."\n";
                    $cuerpomail .= "\n";
                    $insertarqr = $this->QRcode($idevento,$autogenerado,$resultadoticket['idticket'],"ticket");
                    array_push($qrs, $insertarqr);
                }
            }
        }
        //$errores["comidas"] = $comidas;
        foreach ($comidas as $comida) {
            if (isset($comida['cantidad'])) {
                for ($i=0; $i < $comida['cantidad']; $i++) { 

                    $autogenerado = $this->generarCodigo();

                    $insertar = [
                        "idcomida" => $comida['idcomida'],
                        "idevento" => $idevento,
                        "idcompra" => $idcompra,
                        "autogenerado" => $autogenerado
                    ];
                    $resultadocomida = $this->Compracomida_model->insertar($insertar);
                    if($resultadocomida){
                        $cuerpomail .= "La comida numero ".$resultadocomida['identificador']. " codigo de seguridad : ". $resultadocomida['autogenerado']."\n";
                        $cuerpomail .= "\n";
                        $insertarqr = $this->QRcode($idevento,$autogenerado,$resultadocomida['identificador'],"comida");
                    	array_push($qrs, $insertarqr);
                    }
                    else{
                        $errores["ERROR"] = "Error al intentar insertar la comida";   
                    }
                }
            }  
        }
        $detalleevento = $this->Evento_model->detalle($idevento);
        $cuerpomail .= "Sus items son validos para el evento ID: ".$detalleevento['idevento']. " ".$detalleevento['nombre']." para la fecha ".$fecha;
        $destinatario = $this->Client_model->mail($idusuario);
        
        $mail = $this->sendmail($destinatario,$asuntomail, $cuerpomail,$qrs);
        if (!$mail){
            $errores["MAIL"] = "La compra se realizó correctamente, pero el mail no pudo ser enviado, contacte al soporte.";
        } //Cuando se solucione lo del Correo se habilita denuevo
        return $errores;
    }

    public function QRcode($idevento,$autogenerado,$id,$tipo){
    	$this->load->library('ciqrcode');
    	$URL = "http://backend.local/validacion/verificar?idevento=".$idevento."&valor=".$id.":".$autogenerado."&clave=".$tipo."";
    	$params['data'] = $URL;
    	$params['level'] = 'H';
    	$params['size'] = 10;
    	$savename = '/var/www/html/api/imagenes/'.$id.'.png';
    	$params['savename'] = $savename;
    	$this->ciqrcode->generate($params);
    	return $params['savename'];
    }


}