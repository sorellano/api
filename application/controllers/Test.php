<?php
class Test extends CI_Controller {
	public function __construct() {
        parent::__construct();

        $this->load->model('Client_model');
        $this->load->library('unit_test');

    }

    public function index() {
    	$this->unit->set_test_items(array('test_name', 'test_datatype','result', 'line'));
    	echo "<head><title>Unit test API</title><head>";
    	// ---------------------------------- Categoria model
        echo "<hr>Tests de Cliente:";
        $test_name = 'Validar Hash Cliente: Ok';
        $hash = $this->Client_model->generarHash("mail@mail.com", "password");
		$test = $this->Client_model->validarHash($hash);
		$errores = array();
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo print_r($test);
        //----------------------------------------------------------
        $test_name = 'Validar Hash Cliente: null';
        $hash = null;
		$test = $this->Client_model->validarHash($hash);
		$errores = array();
		$errores['HASH_INVALIDO'] = "El hash debe ser texto";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo print_r($test);
		//----------------------------------------------------------
		$test_name = 'Validar Hash Cliente: Invalido';
        $hash = "sdalñk";
		$test = $this->Client_model->validarHash($hash);
		$errores = array();
		$errores['HASH_INVALIDO'] = "El hash debe tener 64 caracteres (sha256)";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo print_r($test);
		//----------------------------------------------------------
		$test_name = 'Validar nombre Cliente: Ok';
        $nombre = "sdalñk";
		$test = $this->Client_model->validarNombre($nombre);
		$errores = array();
		//$errores['HASH_INVALIDO'] = "El hash debe tener 64 caracteres (sha256)";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo print_r($test);
		//----------------------------------------------------------
		$test_name = 'Validar nombre Cliente: Invalido';
        $nombre = null;
		$test = $this->Client_model->validarNombre($nombre);
		$errores = array();
		$errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo print_r($test);
		//----------------------------------------------------------
        $test_name = 'Validar nombre Cliente: Demaciado largo';
        $nombre = "dsalñfksalñdfksalñfksañfkdsaflñsakflsasfadfasdf";
		$test = $this->Client_model->validarNombre($nombre);
		$errores = array();
		$errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 20 caracteres";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo print_r($test);
		//----------------------------------------------------------
		$test_name = 'Validar telefono Cliente:Ok';
        $telefono = "099654375";
		$test = $this->Client_model->validarTelefono($telefono);
		$errores = array();
		//$errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 20 caracteres";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo print_r($test);
		//----------------------------------------------------------
        $test_name = 'Validar telefono Cliente: Demaciado larga';
        $telefono = "3244213412";
		$test = $this->Client_model->validarTelefono($telefono);
		$errores = array();
		$largoTelefono = strlen($telefono);
		$errores['TELEFONO_INVALIDO'] = "Largo del telefono incorrecto, el largo debe ser entre 8 y 9 caracteres y es de ".$largoTelefono." caracteres";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$test_name = 'Validar telefono Cliente: Letras';
        $telefono = "sdafsadf";
		$test = $this->Client_model->validarTelefono($telefono);
		$errores = array();
		$errores['TELEFONO_INVALIDO'] = "El telefono ".$telefono." contiene caracteres no numericos";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$test_name = 'Validar Ciudad Cliente: Ok';
        $ciudad = "Ciudad";
		$test = $this->Client_model->validarCiudad($ciudad);
		$errores = array();
		//$errores['TELEFONO_INVALIDO'] = "El telefono ".$telefono." contiene caracteres no numericos";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$test_name = 'Validar Ciudad Cliente: Null';
        $ciudad = Null;
		$test = $this->Client_model->validarCiudad($ciudad);
		$errores = array();
		$errores['CIUDAD_INVALIDA'] = "El ciudad debe ser texto";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$test_name = 'Validar Ciudad Cliente: Demaciado larga';
        $ciudad = "jksdfhjksdhfkjlsafhlksdjafhaskldfjhsadlkfjsah";
		$test = $this->Client_model->validarCiudad($ciudad);
		$errores = array();
		$errores['CIUDAD_INVALIDA'] = "El ciudad debe ser menor o igual a 20 caracteres";
		$expected_result = $errores;
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
    }

    public function integrationtest() {
    	$this->unit->set_test_items(array('test_name', 'test_datatype','result', 'line'));
    	echo "<head><title>Integration test API</title><head>";
    	echo "<hr>Integration test cliente:<br><br>";
	    //----------------------------------------------------------
		$test_name = 'Integrationtest registro Cliente: Ok!';
		$this->db->query('DELETE FROM CLIENTE WHERE ci="11asd333"');
		$this->db->query('DELETE FROM CLIENTE WHERE ci="11222333" AND mail="test1122@test1122.com"');

		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '099123456',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"exito":true}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest registro Cliente: Repetido';

		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '099123456',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"MAIL_INVALIDO":"El mail ya existe","CI_INVALIDA":"La cedula ya existe","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest registro Cliente: Correo sin dominio';

		$data = array(
	            'mail'      => 'test1122test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '099123456',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"MAIL_INVALIDO":"Los emails deben contener un arroba","CI_INVALIDA":"La cedula ya existe","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;

		//Borro el registro para que sea re utilizable el test-------------------------
		$this->db->query('DELETE FROM CLIENTE WHERE ci="11222333"');
		//----------------------------------------------------------
		$test_name = 'Integrationtest registro Cliente: Correo sin sub-dominio';

		$data = array(
	            'mail'      => 'test1122@test1122',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '099123456',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"MAIL_INVALIDO":"Falta el sub-dominio","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		
		//----------------------------------------------------------
		$test_name = 'Integrationtest registro Cliente: Mail null';

		$data = array(
	            'mail'      => null,
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '099123456',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"MAIL_INVALIDO":"El email debe ser texto","mail_INVALIDO":"El valor mail no puede ser vacio","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest registro Cliente: Nombre null';

		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => null,
	            'apellido'    => 'apellido',
	            'telefono'      => '099123456',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"NOMBRE_INVALIDO":"El nombre debe ser texto","nombre_INVALIDO":"El valor nombre no puede ser vacio","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest registro Cliente: Apellido null';

		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => null,
	            'telefono'      => '099123456',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"APELLIDO_INVALIDO":"El apellido debe ser texto","apellido_INVALIDO":"El valor apellido no puede ser vacio","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest registro Cliente: Telefono caracteres no numericos';

		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => 'dsfgd',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"TELEFONO_INVALIDO":"El telefono '.$data['telefono'].' contiene caracteres no numericos","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest Ciudad Cliente: Ciudad null';
		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '098463214',
	            'ciudad' => null,
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"CIUDAD_INVALIDA":"El ciudad debe ser texto","ciudad_INVALIDO":"El valor ciudad no puede ser vacio","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
    	$test_name = 'Integrationtest Calle Cliente: Calle null';
		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '098563214',
	            'ciudad' => 'ciudad',
	            'calle'    => null,
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"CALLE_INVALIDO":"El calle debe ser texto","calle_INVALIDO":"El valor calle no puede ser vacio","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest Barrio Cliente: Barrio null';
		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '098563214',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => null,
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"BARRIO_INVALIDO":"El barrio debe ser texto","barrio_INVALIDO":"El valor barrio no puede ser vacio","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest CI Cliente: Demaciado larga';
		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '098563214',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '112221333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"CI_INVALIDA":"Largo de la cedula incorrecto","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;

		//----------------------------------------------------------
		$test_name = 'Integrationtest CI Cliente: Caracteres invalidos';
		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '098563214',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11asd333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"CI_INVALIDA":"La cedula contiene caracteres invalidos","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest CI Cliente: CI duplicada';
		$data = array(
	            'mail'      => 'test1123@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '098563214',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => '11222333',
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );

	    $url = 'http://localhost/Clientes/registro';
	    $this->httpPost($url,$data);

		$data['mail'] = 'test1122@test1122.com';

		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"CI_INVALIDA":"La cedula ya existe","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		$this->db->query('DELETE FROM CLIENTE WHERE ci="11222333"');
		//----------------------------------------------------------
		$test_name = 'Integrationtest CI Cliente: Array';
		$data = array(
	            'mail'      => 'test1122@test1122.com',
	            'nombre' => 'nombre',
	            'apellido'    => 'apellido',
	            'telefono'      => '098563214',
	            'ciudad' => 'ciudad',
	            'calle'    => 'calle',
	            'barrio'      => 'barrio',
	            'ci' => array(),
	            'hash'    => '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961'
	    );
	    $url = 'http://localhost/Clientes/registro';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"CI_INVALIDA":"La cedula debe que ser un texto","ci_INVALIDO":"El valor ci no puede ser vacio","exito":false}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Nombre Ok';
		$data = array(
	            'valor'      => 'nombre',
	            'clave' => 'nombre',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"exito":true}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Nombre null';
		$data = array(
	            'valor'      => null,
	            'clave' => 'nombre',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"NOMBRE_INVALIDO":"El nombre debe ser texto"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Nombre demaciado largo';
		$data = array(
	            'valor'      => 'kslajklñsjadklfñjsadfklsajdflksadjflskajflksajdflkjsafñlksajdfñlsakdj',
	            'clave' => 'nombre',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"NOMBRE_INVALIDO":"El nombre debe ser menor o igual a 20 caracteres"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Apellido Ok';
		$data = array(
	            'valor'      => 'apellido',
	            'clave' => 'apellido',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"exito":true}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Apellido null';
		$data = array(
	            'valor'      => null,
	            'clave' => 'apellido',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"APELLIDO_INVALIDO":"El apellido debe ser texto"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Apellido demaciado largo';
		$data = array(
	            'valor'      => 'kslajklñsjadklfñjsadfklsajdflksadjflskajflksajdflkjsafñlksajdfñlsakdj',
	            'clave' => 'apellido',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"APELLIDO_INVALIDO":"El apellido debe ser menor o igual a 20 caracteres"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest telefono Cliente: Telefono Ok';
		$data = array(
	            'valor'      => '096478753',
	            'clave' => 'telefono',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"exito":true}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest telefono Cliente: Telefono demaciado largo';
		$data = array(
	            'valor'      => '0964787323253',
	            'clave' => 'telefono',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"TELEFONO_INVALIDO":"Largo del telefono incorrecto, el largo debe ser entre 8 y 9 caracteres y es de 13 caracteres"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest telefono Cliente: Telefono null';
		$data = array(
	            'valor'      => null,
	            'clave' => 'telefono',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"TELEFONO_INVALIDO":"El telefono '.$data['valor'].' contiene caracteres no numericos"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Ciudad Ok';
		$data = array(
	            'valor'      => 'ciudad',
	            'clave' => 'ciudad',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"exito":true}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Ciudad null';
		$data = array(
	            'valor'      => null,
	            'clave' => 'ciudad',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"CIUDAD_INVALIDA":"El ciudad debe ser texto"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Ciudad demaciado largo';
		$data = array(
	            'valor'      => 'kslajklñsjadklfñjsadfklsajdflksadjflskajflksajdflkjsafñlksajdfñlsakdj',
	            'clave' => 'ciudad',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"CIUDAD_INVALIDA":"El ciudad debe ser menor o igual a 20 caracteres"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Calle Ok';
		$data = array(
	            'valor'      => 'calle',
	            'clave' => 'calle',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"exito":true}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Calle null';
		$data = array(
	            'valor'      => null,
	            'clave' => 'calle',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"CALLE_INVALIDO":"El calle debe ser texto"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Calle demaciado largo';
		$data = array(
	            'valor'      => 'kslajklñsjadklfñjsadfklsajdflksadjflskajflksajdflkjsafñlksajdfñlsakdj',
	            'clave' => 'calle',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"CALLE_INVALIDO":"El calle debe ser menor o igual a 20 caracteres"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Barrio Ok';
		$data = array(
	            'valor'      => 'barrio',
	            'clave' => 'barrio',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"exito":true}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Barrio null';
		$data = array(
	            'valor'      => null,
	            'clave' => 'barrio',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"BARRIO_INVALIDO":"El barrio debe ser texto"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest modificar Cliente: Barrio demaciado largo';
		$data = array(
	            'valor'      => 'kslajklñsjadklfñjsadfklsajdflksadjflskajflksajdflkjsafñlksajdfñlsakdj',
	            'clave' => 'barrio',
	            'idusuario'    => '11222333'
	    );
	    $url = 'http://localhost/Clientes/modificar';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"BARRIO_INVALIDO":"El barrio debe ser menor o igual a 20 caracteres"}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;

		//----------------------------------------------------------

		$test_name = 'Integrationtest Recuperar contraseña: OK';
		$data = array(
	            //'mail'      => 'mail2324@mail.com'
				'mail'      => 'mail@mail.com'
	    );
	    $url = 'http://localhost/Clientes/recuperarPassword';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"exito":true,"info":"Solicitud para recuperar la contrase\u00f1a exitosa, recibira un mail con las indicaciones para recuperar la contrase\u00f1a."}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		
		//----------------------------------------------------------
		$test_name = 'Integrationtest Recuperar contraseña: mail invalido';
		$data = array(
	            'mail'      => '.com'
	    );
	    $url = 'http://localhost/Clientes/recuperarPassword';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"exito":false,"info":"El mail no existe en la base de datos o el usuario esta dado de baja."}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		
		//----------------------------------------------------------
		$test_name = 'Integrationtest Iniciar Sesion: OK';
		$token = '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961';
		$data = array(
	            'token'		=> $token
	    );
	    $url = 'http://localhost/Clientes/login';
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = '{"estado":true}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$this->db->query('UPDATE CLIENTE SET saldo = 10000 WHERE hash = "28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961"');
     
       	$token = '28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961';
		$data = array(
	            'token'		=> $token
	    );
	    // Inicio de sesion
       	$curl = curl_init('http://localhost/Clientes/login');
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_COOKIEJAR, '/tmp/cookies.txt');
		curl_setopt($curl, CURLOPT_COOKIEFILE, '/tmp/cookies.txt');
		curl_setopt($curl, CURLOPT_COOKIE, 'cookiename=cookievalue');
	    $response = curl_exec($curl);
	    

		$test_name = 'Integrationtest Comprar: OK';
		
		$ticket = [];
		$ticket['idtipoticket'] = '94';
    	$ticket['nombre'] = 'Tribuna';
        $ticket['cantidad'] = '1500';
        $ticket['numerado'] = '0';
        $ticket['precio'] = '1';
        $ticket['referencia'] = '';
        $ticket['disponibles'] = '1499';
        $ticket['individuales'] = '1';
		$tickets  = [];
		$tickets[0]= $ticket;

		$items = array( 'tickets' => $tickets );

		$data = Array(
					'fecha' => '2020-12-10 12:00:00',
					'idevento' => '81',
					'metododepago' => '1',
					'items' => $items
				);
	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":true,"info":"Compra exitosa.\n Recibir\u00e1 un mail con los codigos para activar las entradas o reclamar las comidas."}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;


		//----------------------------------------------------------
		
		$test_name = 'Integrationtest Comprar: Cantidad invalida';
		
		$ticket = [];
		$ticket['idtipoticket'] = '94';
    	$ticket['nombre'] = 'Tribuna';
        $ticket['cantidad'] = '1500';
        $ticket['numerado'] = '0';
        $ticket['precio'] = '1';
        $ticket['referencia'] = '';
        $ticket['disponibles'] = '1499';
        $ticket['individuales'] = '456565000';
		$tickets  = [];

		$tickets[0]= $ticket;

		$items = array( 'tickets' => $tickets );

		$data = Array(
					'fecha' => '2020-12-10 12:00:00',
					'idevento' => '81',
					'metododepago' => '1',
					'items' => $items
				);
	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":false,"errores":{"CANTIDAD_INVALIDA 94":"El ticket ID 94 excede el stock presente."}}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest Comprar: Fecha invalida';
		
		$ticket = [];
		$ticket['idtipoticket'] = '94';
    	$ticket['nombre'] = 'Tribuna';
        $ticket['cantidad'] = '1500';
        $ticket['numerado'] = '0';
        $ticket['precio'] = '1';
        $ticket['referencia'] = '';
        $ticket['disponibles'] = '1499';
        $ticket['individuales'] = '1';
		$tickets  = [];

		$tickets[0]= $ticket;

		$items = array( 'tickets' => $tickets );

		$data = Array(
					'fecha' => '2025-12-10 12:00:00',
					'idevento' => '81',
					'metododepago' => '1',
					'items' => $items
				);
	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":false,"errores":{"FECHA_INVALIDA":"La fecha de los tickets no coincide con la fecha enviada"}}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest Comprar: IdTipoTicket invalido';
		
		$ticket = [];
		$ticket['idtipoticket'] = '849489494';
    	$ticket['nombre'] = 'Tribuna';
        $ticket['cantidad'] = '1500';
        $ticket['numerado'] = '0';
        $ticket['precio'] = '1';
        $ticket['referencia'] = '';
        $ticket['disponibles'] = '1499';
        $ticket['individuales'] = '1';
		$tickets  = [];

		$tickets[0]= $ticket;

		$items = array( 'tickets' => $tickets );

		$data = Array(
					'fecha' => '2020-12-10 12:00:00',
					'idevento' => '81',
					'metododepago' => '1',
					'items' => $items
				);
	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":false,"errores":{"TICKET_INVALIDO 849489494":"El ticket que se desea comprar no es valido."}}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest Comprar: Ticket no numerado enviado como numerado';
		
		$ticket = [];
		$ticket['idtipoticket'] = '94';
    	$ticket['nombre'] = 'Tribuna';
        $ticket['cantidad'] = '1500';
        $ticket['numerado'] = '1';
        $ticket['precio'] = '1';
        $ticket['referencia'] = '';
        $ticket['disponibles'] = '1499';
        $ticket['individuales'] = '1';
		$tickets  = [];

		$tickets[0]= $ticket;

		$items = array( 'tickets' => $tickets );

		$data = Array(
					'fecha' => '2020-12-10 12:00:00',
					'idevento' => '81',
					'metododepago' => '1',
					'items' => $items
				);
	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":false,"errores":{"ERROR":"Error no se encuentra el numero de los asientos numerados elegidos"}}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest Comprar: idevento invalido';
		
		$ticket = [];
		$ticket['idtipoticket'] = '94';
    	$ticket['nombre'] = 'Tribuna';
        $ticket['cantidad'] = '1500';
        $ticket['numerado'] = '0';
        $ticket['precio'] = '1';
        $ticket['referencia'] = '';
        $ticket['disponibles'] = '1499';
        $ticket['individuales'] = '1';
		$tickets  = [];

		$tickets[0]= $ticket;

		$items = array( 'tickets' => $tickets );

		$data = Array(
					'fecha' => '2020-12-10 12:00:00',
					'idevento' => '5456456465',
					'metododepago' => '1',
					'items' => $items
				);
	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":false,"errores":{"Evento":"El evento ID 5456456465 parece no estar dado de alta."}}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest Comprar: idevento valido (El ticket no pertenece al evento)';
		
		$ticket = [];
		$ticket['idtipoticket'] = '94';
    	$ticket['nombre'] = 'Tribuna';
        $ticket['cantidad'] = '1500';
        $ticket['numerado'] = '0';
        $ticket['precio'] = '1';
        $ticket['referencia'] = '';
        $ticket['disponibles'] = '1499';
        $ticket['individuales'] = '1';
		$tickets  = [];

		$tickets[0]= $ticket;

		$items = array( 'tickets' => $tickets );

		$data = Array(
					'fecha' => '2020-12-10 12:00:00',
					'idevento' => '69',
					'metododepago' => '1',
					'items' => $items
				);
	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":false,"errores":{"TICKET_INVALIDO 94":"El ticket que se desea comprar no es valido."}}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest Comprar: Metodo de pago invalido';
		
		$ticket = [];
		$ticket['idtipoticket'] = '94';
    	$ticket['nombre'] = 'Tribuna';
        $ticket['cantidad'] = '1500';
        $ticket['numerado'] = '0';
        $ticket['precio'] = '1';
        $ticket['referencia'] = '';
        $ticket['disponibles'] = '1499';
        $ticket['individuales'] = '1';
		$tickets  = [];

		$tickets[0]= $ticket;

		$items = array( 'tickets' => $tickets );

		$data = Array(
					'fecha' => '2020-12-10 12:00:00',
					'idevento' => '81',
					'metododepago' => '7498498498498',
					'items' => $items
				);
	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":false,"errores":{"Metodo de pago":"El metodo de pago ID 7498498498498 parece no estar dado de alta"}}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------

		$test_name = 'Integrationtest Comprar: Sin saldo';
		$this->db->query('UPDATE CLIENTE SET saldo = 0 WHERE hash = "28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961"');
		$ticket = [];
		$ticket['idtipoticket'] = '94';
    	$ticket['nombre'] = 'Tribuna';
        $ticket['cantidad'] = '1500';
        $ticket['numerado'] = '0';
        $ticket['precio'] = '1';
        $ticket['referencia'] = '';
        $ticket['disponibles'] = '1499';
        $ticket['individuales'] = '1';
		$tickets  = [];

		$tickets[0]= $ticket;

		$items = array( 'tickets' => $tickets );

		$data = Array(
					'fecha' => '2020-12-10 12:00:00',
					'idevento' => '81',
					'metododepago' => '1',
					'items' => $items
				);
	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":false,"errores":{"SALDO_INSUFICIENTE":"El saldo disponible no es suficiente para realizar la compra: Saldo 0 Subtotal 150"}}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		$this->db->query('UPDATE CLIENTE SET saldo = 100000 WHERE hash = "28376a1433f844e72c90da01bfcc8f1621d94468ae0182d2d92a4c2e9a531961"');
		//----------------------------------------------------------
		$test_name = 'Integrationtest Comprar: Compra compleja';

		$data = [
		"fecha" => "2021-03-11 12:00:00",
		"idevento" => "72",
		"metododepago" => "1",
		"items[paquetes][0][precio]" => "19.6",
		"items[paquetes][0][comidas][0][idpaquete]" => "2",
		"items[paquetes][0][comidas][0][idcomida]" => "7",
		"items[paquetes][0][comidas][0][cantidad]" => "1",
		"items[paquetes][0][comidas][0][descuento]" => "50",
		"items[paquetes][0][comidas][0][nombre]" => "BMT+Italiano",
		"items[paquetes][0][comidas][0][descripcion]" => "BMT+Italiano",
		"items[paquetes][0][comidas][0][baja]" => "0",
		"items[paquetes][0][comidas][0][imagen]" => "",
		"items[paquetes][0][comidas][0][idevento]" => "72",
		"items[paquetes][0][comidas][0][precio]" => "20",
		"items[paquetes][0][metodosdepago][0][idpaquete]" => "2",
		"items[paquetes][0][metodosdepago][0][idmetodopago]" => "1",
		"items[paquetes][0][metodosdepago][0][nombre]" => "BANDES",
		"items[paquetes][0][metodosdepago][0][descripcion]" => "BANDES",
		"items[paquetes][0][metodosdepago][0][baja]" => "0",
		"items[paquetes][0][metodosdepago][0][logo]" => "bandes-uruguay-blanco.jpg",
		"items[paquetes][0][metodosdepago][1][idpaquete]" => "2",
		"items[paquetes][0][metodosdepago][1][idmetodopago]" => "2",
		"items[paquetes][0][metodosdepago][1][nombre]" => "BBVA",
		"items[paquetes][0][metodosdepago][1][descripcion]" => "BBVA",
		"items[paquetes][0][metodosdepago][1][baja]" => "0",
		"items[paquetes][0][metodosdepago][1][logo]" => "1556121045_848282_1556124517_noticia_normal.jpg",
		"items[paquetes][0][metodosdepago][2][idpaquete]" => "2",
		"items[paquetes][0][metodosdepago][2][idmetodopago]" => "3",
		"items[paquetes][0][metodosdepago][2][nombre]" => "Banque+Heritage",
		"items[paquetes][0][metodosdepago][2][descripcion]" => "Banque+Heritage",
		"items[paquetes][0][metodosdepago][2][baja]" => "0",
		"items[paquetes][0][metodosdepago][2][logo]" => "banqueheritage-1.jpg",
		"items[paquetes][0][metodosdepago][3][idpaquete]" => "2",
		"items[paquetes][0][metodosdepago][3][idmetodopago]" => "4",
		"items[paquetes][0][metodosdepago][3][nombre]" => "HSBC",
		"items[paquetes][0][metodosdepago][3][descripcion]" => "HSBC",
		"items[paquetes][0][metodosdepago][3][baja]" => "0",
		"items[paquetes][0][metodosdepago][3][logo]" => "HSBC-LOGO.jpg",
		"items[paquetes][0][metodosdepago][4][idpaquete]" => "2",
		"items[paquetes][0][metodosdepago][4][idmetodopago]" => "5",
		"items[paquetes][0][metodosdepago][4][nombre]" => "Itaú",
		"items[paquetes][0][metodosdepago][4][descripcion]" => "Itaú",
		"items[paquetes][0][metodosdepago][4][baja]" => "0",
		"items[paquetes][0][metodosdepago][4][logo]" => "1973722.png",
		"items[paquetes][0][metodosdepago][5][idpaquete]" => "2",
		"items[paquetes][0][metodosdepago][5][idmetodopago]" => "6",
		"items[paquetes][0][metodosdepago][5][nombre]" => "Visa",
		"items[paquetes][0][metodosdepago][5][descripcion]" => "Visa",
		"items[paquetes][0][metodosdepago][5][baja]" => "0",
		"items[paquetes][0][metodosdepago][5][logo]" => "VISA-Logo3.jpg",
		"items[paquetes][0][idpaquete]" => "2",
		"items[paquetes][0][idevento]" => "72",
		"items[paquetes][0][nombre]" => "otro+paquete",
		"items[paquetes][0][baja]" => "0",
		"items[paquetes][0][fecha]" => "2021-03-11 12:00:00",
		"items[paquetes][0][imagen]" => "",
		"items[paquetes][0][stock]" => "Infinity",
		"items[paquetes][0][cantidadelegida]" => "2",
		"items[paquetes][1][precio]" => "78.4",
		"items[paquetes][1][comidas][0][idpaquete]" => "3",
		"items[paquetes][1][comidas][0][idcomida]" => "1",
		"items[paquetes][1][comidas][0][cantidad]" => "2",
		"items[paquetes][1][comidas][0][descuento]" => "50",
		"items[paquetes][1][comidas][0][nombre]" => "Alas+de+Pollo",
		"items[paquetes][1][comidas][0][descripcion]" => "Alas+de+Pollo",
		"items[paquetes][1][comidas][0][baja]" => "0",
		"items[paquetes][1][comidas][0][imagen]" => "",
		"items[paquetes][1][comidas][0][idevento]" => "72",
		"items[paquetes][1][comidas][0][precio]" => "20",
		"items[paquetes][1][comidas][1][idpaquete]" => "3",
		"items[paquetes][1][comidas][1][idcomida]" => "2",
		"items[paquetes][1][comidas][1][cantidad]" => "1",
		"items[paquetes][1][comidas][1][descuento]" => "50",
		"items[paquetes][1][comidas][1][nombre]" => "Aros+de+cebolla",
		"items[paquetes][1][comidas][1][descripcion]" => "Aros+de+cebolla",
		"items[paquetes][1][comidas][1][baja]" => "0",
		"items[paquetes][1][comidas][1][imagen]" => "aros-de-cebolla.jpg",
		"items[paquetes][1][comidas][1][idevento]" => "72",
		"items[paquetes][1][comidas][1][precio]" => "20",
		"items[paquetes][1][comidas][2][idpaquete]" => "3",
		"items[paquetes][1][comidas][2][idcomida]" => "12",
		"items[paquetes][1][comidas][2][cantidad]" => "1",
		"items[paquetes][1][comidas][2][descuento]" => "50",
		"items[paquetes][1][comidas][2][nombre]" => "Burrito+Taco+Bell",
		"items[paquetes][1][comidas][2][descripcion]" => "Burrito+Taco+Bell",
		"items[paquetes][1][comidas][2][baja]" => "0",
		"items[paquetes][1][comidas][2][imagen]" => "",
		"items[paquetes][1][comidas][2][idevento]" => "72",
		"items[paquetes][1][comidas][2][precio]" => "20",
		"items[paquetes][1][metodosdepago][0][idpaquete]" => "3",
		"items[paquetes][1][metodosdepago][0][idmetodopago]" => "1",
		"items[paquetes][1][metodosdepago][0][nombre]" => "BANDES",
		"items[paquetes][1][metodosdepago][0][descripcion]" => "BANDES",
		"items[paquetes][1][metodosdepago][0][baja]" => "0",
		"items[paquetes][1][metodosdepago][0][logo]" => "bandes-uruguay-blanco.jpg",
		"items[paquetes][1][idpaquete]" => "3",
		"items[paquetes][1][idevento]" => "72",
		"items[paquetes][1][nombre]" => "otro+paquete+mas",
		"items[paquetes][1][baja]" => "0",
		"items[paquetes][1][fecha]" => "2021-03-11 12:00:00",
		"items[paquetes][1][imagen]" => "",
		"items[paquetes][1][stock]" => "Infinity",
		"items[paquetes][1][cantidadelegida]" => "1",
		"items[paquetes][2][precio]" => "19.6",
		"items[paquetes][2][comidas][0][idpaquete]" => "4",
		"items[paquetes][2][comidas][0][idcomida]" => "1",
		"items[paquetes][2][comidas][0][cantidad]" => "1",
		"items[paquetes][2][comidas][0][descuento]" => "50",
		"items[paquetes][2][comidas][0][nombre]" => "Alas+de+Pollo",
		"items[paquetes][2][comidas][0][descripcion]" => "Alas+de+Pollo",
		"items[paquetes][2][comidas][0][baja]" => "0",
		"items[paquetes][2][comidas][0][imagen]" => "",
		"items[paquetes][2][comidas][0][idevento]" => "72",
		"items[paquetes][2][comidas][0][precio]" => "20",
		"items[paquetes][2][metodosdepago][0][idpaquete]" => "4",
		"items[paquetes][2][metodosdepago][0][idmetodopago]" => "1",
		"items[paquetes][2][metodosdepago][0][nombre]" => "BANDES",
		"items[paquetes][2][metodosdepago][0][descripcion]" => "BANDES",
		"items[paquetes][2][metodosdepago][0][baja]" => "0",
		"items[paquetes][2][metodosdepago][0][logo]" => "bandes-uruguay-blanco.jpg",
		"items[paquetes][2][metodosdepago][1][idpaquete]" => "4",
		"items[paquetes][2][metodosdepago][1][idmetodopago]" => "2",
		"items[paquetes][2][metodosdepago][1][nombre]" => "BBVA",
		"items[paquetes][2][metodosdepago][1][descripcion]" => "BBVA",
		"items[paquetes][2][metodosdepago][1][baja]" => "0",
		"items[paquetes][2][metodosdepago][1][logo]" => "1556121045_848282_1556124517_noticia_normal.jpg",
		"items[paquetes][2][metodosdepago][2][idpaquete]" => "4",
		"items[paquetes][2][metodosdepago][2][idmetodopago]" => "3",
		"items[paquetes][2][metodosdepago][2][nombre]" => "Banque+Heritage",
		"items[paquetes][2][metodosdepago][2][descripcion]" => "Banque+Heritage",
		"items[paquetes][2][metodosdepago][2][baja]" => "0",
		"items[paquetes][2][metodosdepago][2][logo]" => "banqueheritage-1.jpg",
		"items[paquetes][2][idpaquete]" => "4",
		"items[paquetes][2][idevento]" => "72",
		"items[paquetes][2][nombre]" => "paquete+mas",
		"items[paquetes][2][baja]" => "0",
		"items[paquetes][2][fecha]" => "2021-03-11 12:00:00",
		"items[paquetes][2][imagen]" => "",
		"items[paquetes][2][stock]" => "Infinity",
		"items[paquetes][2][cantidadelegida]" => "1",
		"items[comidas][0][nombrecomida]" => "Alas+de+Pollo",
		"items[comidas][0][precio]" => "20",
		"items[comidas][0][idcomida]" => "1",
		"items[comidas][0][imagen]" => "",
		"items[comidas][0][cantidad]" => "3",
		"items[comidas][1][nombrecomida]" => "Big+Mac+de+McDonald's",
		"items[comidas][1][precio]" => "20",
		"items[comidas][1][idcomida]" => "6",
		"items[comidas][1][imagen]" => "",
		"items[comidas][1][cantidad]" => "2",
		"items[comidas][2][nombrecomida]" => "CheeseBurguer",
		"items[comidas][2][precio]" => "20",
		"items[comidas][2][idcomida]" => "13",
		"items[comidas][2][imagen]" => "",
		"items[comidas][2][cantidad]" => "2",
		"items[tickets][0][idtipoticket]" => "113",
		"items[tickets][0][nombre]" => "entrada+3",
		"items[tickets][0][cantidad]" => "1800",
		"items[tickets][0][numerado]" => "0",
		"items[tickets][0][precio]" => "10",
		"items[tickets][0][referencia]" => "",
		"items[tickets][0][disponibles]" => "1796",
		"items[tickets][0][individuales]" => "5",
		"items[tickets][1][idtipoticket]" => "115",
		"items[tickets][1][nombre]" => "entrada+6",
		"items[tickets][1][cantidad]" => "10",
		"items[tickets][1][numerado]" => "0",
		"items[tickets][1][precio]" => "10",
		"items[tickets][1][referencia]" => "",
		"items[tickets][1][disponibles]" => "9",
		"items[tickets][1][individuales]" => "3",
		"items[tickets][2][idtipoticket]" => "118",
		"items[tickets][2][nombre]" => "entrada+8",
		"items[tickets][2][cantidad]" => "10",
		"items[tickets][2][numerado]" => "0",
		"items[tickets][2][precio]" => "10",
		"items[tickets][2][referencia]" => "",
		"items[tickets][2][disponibles]" => "10",
		"items[tickets][2][individuales]" => "2"];

	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":true,"info":"Compra exitosa.\n Recibir\u00e1 un mail con los codigos para activar las entradas o reclamar las comidas."}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest Comprar: Comprar Paquetes';

		$data = [
			"fecha" => "2021-03-11 12:00:00",
			"idevento" => "72",
			"metododepago" => "1",
			"items[paquetes][0][precio]" => "19.6",
			"items[paquetes][0][comidas][0][idpaquete]" => "2",
			"items[paquetes][0][comidas][0][idcomida]" => "7",
			"items[paquetes][0][comidas][0][cantidad]" => "1",
			"items[paquetes][0][comidas][0][descuento]" => "50",
			"items[paquetes][0][comidas][0][nombre]" => "BMT+Italiano",
			"items[paquetes][0][comidas][0][descripcion]" => "BMT+Italiano",
			"items[paquetes][0][comidas][0][baja]" => "0",
			"items[paquetes][0][comidas][0][imagen]" => "",
			"items[paquetes][0][comidas][0][idevento]" => "72",
			"items[paquetes][0][comidas][0][precio]" => "20",
			"items[paquetes][0][metodosdepago][0][idpaquete]" => "2",
			"items[paquetes][0][metodosdepago][0][idmetodopago]" => "1",
			"items[paquetes][0][metodosdepago][0][nombre]" => "BANDES",
			"items[paquetes][0][metodosdepago][0][descripcion]" => "BANDES",
			"items[paquetes][0][metodosdepago][0][baja]" => "0",
			"items[paquetes][0][metodosdepago][0][logo]" => "bandes-uruguay-blanco.jpg",
			"items[paquetes][0][metodosdepago][1][idpaquete]" => "2",
			"items[paquetes][0][metodosdepago][1][idmetodopago]" => "2",
			"items[paquetes][0][metodosdepago][1][nombre]" => "BBVA",
			"items[paquetes][0][metodosdepago][1][descripcion]" => "BBVA",
			"items[paquetes][0][metodosdepago][1][baja]" => "0",
			"items[paquetes][0][metodosdepago][1][logo]" => "1556121045_848282_1556124517_noticia_normal.jpg",
			"items[paquetes][0][metodosdepago][2][idpaquete]" => "2",
			"items[paquetes][0][metodosdepago][2][idmetodopago]" => "3",
			"items[paquetes][0][metodosdepago][2][nombre]" => "Banque+Heritage",
			"items[paquetes][0][metodosdepago][2][descripcion]" => "Banque+Heritage",
			"items[paquetes][0][metodosdepago][2][baja]" => "0",
			"items[paquetes][0][metodosdepago][2][logo]" => "banqueheritage-1.jpg",
			"items[paquetes][0][metodosdepago][3][idpaquete]" => "2",
			"items[paquetes][0][metodosdepago][3][idmetodopago]" => "4",
			"items[paquetes][0][metodosdepago][3][nombre]" => "HSBC",
			"items[paquetes][0][metodosdepago][3][descripcion]" => "HSBC",
			"items[paquetes][0][metodosdepago][3][baja]" => "0",
			"items[paquetes][0][metodosdepago][3][logo]" => "HSBC-LOGO.jpg",
			"items[paquetes][0][metodosdepago][4][idpaquete]" => "2",
			"items[paquetes][0][metodosdepago][4][idmetodopago]" => "5",
			"items[paquetes][0][metodosdepago][4][nombre]" => "Itaú",
			"items[paquetes][0][metodosdepago][4][descripcion]" => "Itaú",
			"items[paquetes][0][metodosdepago][4][baja]" => "0",
			"items[paquetes][0][metodosdepago][4][logo]" => "1973722.png",
			"items[paquetes][0][metodosdepago][5][idpaquete]" => "2",
			"items[paquetes][0][metodosdepago][5][idmetodopago]" => "6",
			"items[paquetes][0][metodosdepago][5][nombre]" => "Visa",
			"items[paquetes][0][metodosdepago][5][descripcion]" => "Visa",
			"items[paquetes][0][metodosdepago][5][baja]" => "0",
			"items[paquetes][0][metodosdepago][5][logo]" => "VISA-Logo3.jpg",
			"items[paquetes][0][idpaquete]" => "2",
			"items[paquetes][0][idevento]" => "72",
			"items[paquetes][0][nombre]" => "otro+paquete",
			"items[paquetes][0][baja]" => "0",
			"items[paquetes][0][fecha]" => "2021-03-11 12:00:00",
			"items[paquetes][0][imagen]" => "",
			"items[paquetes][0][stock]" => "Infinity",
			"items[paquetes][0][cantidadelegida]" => "2",
			"items[paquetes][1][precio]" => "78.4",
			"items[paquetes][1][comidas][0][idpaquete]" => "3",
			"items[paquetes][1][comidas][0][idcomida]" => "1",
			"items[paquetes][1][comidas][0][cantidad]" => "2",
			"items[paquetes][1][comidas][0][descuento]" => "50",
			"items[paquetes][1][comidas][0][nombre]" => "Alas+de+Pollo",
			"items[paquetes][1][comidas][0][descripcion]" => "Alas+de+Pollo",
			"items[paquetes][1][comidas][0][baja]" => "0",
			"items[paquetes][1][comidas][0][imagen]" => "",
			"items[paquetes][1][comidas][0][idevento]" => "72",
			"items[paquetes][1][comidas][0][precio]" => "20",
			"items[paquetes][1][comidas][1][idpaquete]" => "3",
			"items[paquetes][1][comidas][1][idcomida]" => "2",
			"items[paquetes][1][comidas][1][cantidad]" => "1",
			"items[paquetes][1][comidas][1][descuento]" => "50",
			"items[paquetes][1][comidas][1][nombre]" => "Aros+de+cebolla",
			"items[paquetes][1][comidas][1][descripcion]" => "Aros+de+cebolla",
			"items[paquetes][1][comidas][1][baja]" => "0",
			"items[paquetes][1][comidas][1][imagen]" => "aros-de-cebolla.jpg",
			"items[paquetes][1][comidas][1][idevento]" => "72",
			"items[paquetes][1][comidas][1][precio]" => "20",
			"items[paquetes][1][comidas][2][idpaquete]" => "3",
			"items[paquetes][1][comidas][2][idcomida]" => "12",
			"items[paquetes][1][comidas][2][cantidad]" => "1",
			"items[paquetes][1][comidas][2][descuento]" => "50",
			"items[paquetes][1][comidas][2][nombre]" => "Burrito+Taco+Bell",
			"items[paquetes][1][comidas][2][descripcion]" => "Burrito+Taco+Bell",
			"items[paquetes][1][comidas][2][baja]" => "0",
			"items[paquetes][1][comidas][2][imagen]" => "",
			"items[paquetes][1][comidas][2][idevento]" => "72",
			"items[paquetes][1][comidas][2][precio]" => "20",
			"items[paquetes][1][metodosdepago][0][idpaquete]" => "3",
			"items[paquetes][1][metodosdepago][0][idmetodopago]" => "1",
			"items[paquetes][1][metodosdepago][0][nombre]" => "BANDES",
			"items[paquetes][1][metodosdepago][0][descripcion]" => "BANDES",
			"items[paquetes][1][metodosdepago][0][baja]" => "0",
			"items[paquetes][1][metodosdepago][0][logo]" => "bandes-uruguay-blanco.jpg",
			"items[paquetes][1][idpaquete]" => "3",
			"items[paquetes][1][idevento]" => "72",
			"items[paquetes][1][nombre]" => "otro+paquete+mas",
			"items[paquetes][1][baja]" => "0",
			"items[paquetes][1][fecha]" => "2021-03-11 12:00:00",
			"items[paquetes][1][imagen]" => "",
			"items[paquetes][1][stock]" => "Infinity",
			"items[paquetes][1][cantidadelegida]" => "1"];

	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":true,"info":"Compra exitosa.\n Recibir\u00e1 un mail con los codigos para activar las entradas o reclamar las comidas."}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------
		$test_name = 'Integrationtest Comprar: Comprar paquete';
		
		$data = [
			"fecha" => "2021-03-11 12:00:00",
			"idevento" => "72",
			"metododepago" => "1",
			"items[comidas][0][nombrecomida]" => "Alas+de+Pollo",
			"items[comidas][0][precio]" => "20",
			"items[comidas][0][idcomida]" => "1",
			"items[comidas][0][imagen]" => "",
			"items[comidas][0][cantidad]" => "3",
			"items[comidas][1][nombrecomida]" => "Big+Mac+de+McDonald's",
			"items[comidas][1][precio]" => "20",
			"items[comidas][1][idcomida]" => "6",
			"items[comidas][1][imagen]" => "",
			"items[comidas][1][cantidad]" => "2",
			"items[comidas][2][nombrecomida]" => "CheeseBurguer",
			"items[comidas][2][precio]" => "20",
			"items[comidas][2][idcomida]" => "13",
			"items[comidas][2][imagen]" => "",
			"items[comidas][2][cantidad]" => "2"];

	    $url = 'http://localhost/Eventos/comprar';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
		$test = $response;
		$expected_result = '{"exito":true,"info":"Compra exitosa.\n Recibir\u00e1 un mail con los codigos para activar las entradas o reclamar las comidas."}';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------

		//----------------------------------------------------------
		curl_close($curl); //cierro la sesion
		//----------------------------------------------------------


		$test_name = 'Integrationtest Cerrar Sesion: OK';
	    $url = 'http://localhost/Clientes/logout';
	    $data = array();
		$test = $this->httpPost($url,$data);
		$errores = array();
		$expected_result = 'true';
		echo $this->unit->run($test, $expected_result, $test_name);
		echo $test;
		//----------------------------------------------------------

	}

	public function httpPost($url, $data)
	{
	    $curl = curl_init($url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
	    curl_close($curl);
	    return $response;
	}

}
