<?php
class Users extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Client_model');

    }

	public function index() {
		header("Content-Type: application/json");
		echo(true);
	}

	public function baja() {
		// Carga el body de la request y lo decodea como JSON a mano
		// para que el codigo funcione copiando y pegando sin configurar el rewrite de headers
		$json = json_decode(file_get_contents('php://input'), true);

		header("Content-Type: application/json");

		$cliente = $this->Client_model->getClienteAutenticadoByHash( $json['auth'] );

		if ($cliente)
			echo( $this->Client_model->darDeBaja($cliente) );
		else
			echo( json_encode(false) );

	}

	public function registro() {

		// Carga el body de la request y lo decodea como JSON a mano
		// para que el codigo funcione copiando y pegando sin configurar el rewrite de headers
		$json = json_decode(file_get_contents('php://input'), true);

		$cliente = [
			'mail'     => $json['mail'],
			'nombre'   => $json['nombre'],
			'apellido' => $json['apellido'],
			'telefono' => $json['telefono'],
			'ciudad'   => $json['ciudad'],
			'calle'    => $json['calle'],
			'barrio'   => $json['barrio'],
			'ci'       => $json['ci'],

			'hash' => $this->Client_model->generarHash($json['mail'], $json['password'])
		];

		$errores = $this->Client_model->validarInformacion($cliente);

		header("Content-Type: application/json");
		if ( count($errores) > 0 )
			echo( json_encode($errores) );
		else {
			$resultado = $this->Client_model->insertar($cliente);
			echo $resultado;
		}

	}

	public function modificar() {

		// Carga el body de la request y lo decodea como JSON a mano
		// para que el codigo funcione copiando y pegando sin configurar el rewrite de headers
		$json = json_decode(file_get_contents('php://input'), true);

		header("Content-Type: application/json");

		$cliente = $this->Client_model->getClienteAutenticadoByHash($json['auth']);

		if (!$cliente) {
			echo( json_encode(false) );
			return;
		}

		if ( isset( $json['nombre'] ) )
			$cliente["nombre"] = $json['nombre'];

		if ( isset( $json['apellido'] ) )
			$cliente["apellido"] = $json['apellido'];

		if ( isset( $json['telefono'] ) )
			$cliente["telefono"] = $json['telefono'];

		if ( isset( $json['ciudad'] ) )
			$cliente["ciudad"] = $json['ciudad'];

		if ( isset( $json['calle'] ) )
			$cliente["calle"] = $json['calle'];

		if ( isset( $json['barrio'] ) )
			$cliente["barrio"] = $json['barrio'];

		if ( isset( $json['ci'] ) )
			$cliente["ci"] = $json['ci'];

		$errores = $this->Client_model->validarInformacion($cliente);


		if ( count($errores) > 0 )
			echo( json_encode($errores) );
		else
			echo( json_encode( $this->Client_model->actualizar($cliente) ) );

	}
}
