<?php
class Lugar extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Lugar_model');

    }

    public function listar() {

        $lugares = $this->Lugar_model->listar();
        echo(json_encode($lugares));

        /*
    	if($this->session->userdata('logeado')){
    		$lugares = $this->Lugar_model->listar();
    	echo(json_encode($lugares));
    	}
    	else{
    		echo(json_encode(false));
    	}*/
    	
    }

}