<?php

class Clientes extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Client_model');

    }

	public function login() {

		$token = $this->input->post('token');

		$cliente = $this->Client_model->autenticarPorHash($token);

		if (!$cliente) {
			$info = 'Error en los parametros de inicio de sesion';

			$data = [
				'info' => $info
			];
		}
		else{
			$session = array(
	            'idusuario' => $cliente['idusuario'],
	            'logeado'   => TRUE
       		);
       		$this->session->set_userdata($session);

			$data = [
				'estado' => $this->session->userdata('logeado')
			];
		}		
		echo(json_encode($data));
	}

	public function estado() { //Estado retorna al usuario el id de usuario de su sesion o null si no esta loggeado.
		echo(json_encode($this->session->userdata('idusuario')));
	}

	public function logout() {
        $this->session->sess_destroy();
		echo(json_encode(TRUE));
    }

	public function registro() {
		

		$cliente = [
			'mail'     => $this->input->post('mail'),
			'nombre'   => $this->input->post('nombre'),
			'apellido' => $this->input->post('apellido'),
			'telefono' => $this->input->post('telefono'),
			'ciudad'   => $this->input->post('ciudad'),
			'calle'    => $this->input->post('calle'),
			'barrio'   => $this->input->post('barrio'),
			'ci'       => $this->input->post('ci'),
			'hash' => $this->input->post('hash')
			//'hash' => $this->Client_model->generarHash($this->input->post('mail'), $this->input->post('password'))
		];

		$errores = $this->Client_model->validarInformacion($cliente);

		foreach ($cliente as $key => $value) {
			if (strlen($value) <= 0) {
				$errores[$key."_INVALIDO"] = "El valor ".$key." no puede ser vacio";
			}
		}

		if ( count($errores) > 0 ) {
			$errores['exito'] = false;
			echo( json_encode($errores) );
		} else {
			//unset($cliente['password']);
			$resultado = $this->Client_model->insertar($cliente);
			echo ( json_encode(['exito' => $resultado]) );
		}

	}



	public function getPerfil() {

		$idusuario = $this->session->userdata('idusuario');

		if ($this->Client_model->existeIdUsuario($idusuario)) {
			if($perfil = $this->Client_model->perfilPorId($idusuario)){
				unset( $perfil['hash'] );
				unset( $perfil['baja'] );
				echo ( json_encode($perfil) );
			}
			else{
				echo ( json_encode(['Error' => 'Error al intentar cargar la informacion de perfil, no se encuentra el perfil para el id de usuario']) );
			}		
		}
		else{
			echo ( json_encode(['Error' => 'Error al intentar cargar la informacion de perfil, no se encuentra el id de usuario']) );
		}

	}



	public function baja() {
		$idusuario = $this->session->userdata('idusuario');
		if($this->Client_model->darDeBaja($idusuario)){
			$this->logout();
		}
		else{
			echo(json_encode(TRUE));
		}
	}

	public function modificar() {
		$valor     = $this->input->post('valor');
        $clave     = $this->input->post('clave');
        $idusuario = $this->session->userdata('idusuario');


        if($clave=="nombre")
        	{$errores = $this->Client_model->validarNombre($valor);}
        if($clave=="apellido")
        	{$errores = $this->Client_model->validarApellido($valor);}
        if($clave=="telefono")
        	{$errores = $this->Client_model->validarTelefono($valor);}
        if($clave=="ciudad")
        	{$errores = $this->Client_model->validarCiudad($valor);}
        if($clave=="calle")
        	{$errores = $this->Client_model->validarCalle($valor);}
        if($clave=="barrio")
        	{$errores = $this->Client_model->validarBarrio($valor);}
        if($valor==="")
        	{$errores['VALOR_VACIO']= 'El valor no puede estar vacio';}
        if ( count($errores) > 0) {
            echo( json_encode($errores) );
        }
        else{
	        if ($this->Client_model->modificarCliente($idusuario,$clave,$valor)) {
	        	echo( json_encode(['exito' => true]) );
	        }
	        else{
	        	echo( json_encode(['exito' => false]) );
	        }
	    }
	}

	public function cambiarcontrasena() {
		$password     = $this->input->post('password');
		$idusuario = $this->session->userdata('idusuario');
		$cliente = $this->Client_model->perfilPorId($idusuario);
		$hash = $this->Client_model->generarHash($cliente['mail'], $password);

		if($password===""){
			$errores = ['CONTRASEÑA_INVALIDA' => 'La contraseña no puede ser vacia'];
			echo(json_encode(['errores' => $errores]));
		}
		elseif($this->Client_model->modificarCliente($idusuario,'hash',$hash)){
			echo( json_encode(['exito' => true]) );
		}
		else{
			echo( json_encode(['exito' => false]) );	
		}


	}

	public function solicitarsaldo() {
		$saldo     		= $this->input->post('saldo');
		$metododepago   = $this->input->post('metododepago');
		$idusuario 		= $this->session->userdata('idusuario');

		if($saldo==="" || $metododepago===""){
			$errores = ['SALDO_INVALIDO' => 'El saldo no puede estar vacio'];
			echo(json_encode(['errores' => $errores]));
		}
		else{
			$errores = $this->Client_model->validarSaldo($saldo);
			if ( count($errores) > 0) {
            	echo( json_encode($saldo) );
        	}
        	else{
        		$solicitar = [
        			'idusuario' => $idusuario,
        			'saldo' => $saldo,
        			'idmetodopago' => $metododepago
        		];
		        if ($this->Client_model->solicitarSaldo($solicitar)) {
		        	echo( json_encode(['exito' => true]) );
		        }
		        else{
		        	echo( json_encode(['exito' => false]) );
		        }
	    	}
		}	
	}

	public function recuperarPassword() { // el cliente espera la variable exito para evaluar si se pudo o no, e info para mostrar el mensaje
		$mail = $this->input->post('mail');
		$idusuario = $this->Client_model->idusuarioPorMail($mail);
		
		if ($idusuario) {
			
			$expira = date('Y-m-d', strtotime(' +1 day'));
			$autogenerado = $this->generarCodigo();

			$recuperacion = [
				"idusuario" => $idusuario,
				"expira" => $expira,
				"autogenerado" => $autogenerado
			];
			$resultadoRecuperacion = $this->Client_model->insertarRecuperacion($recuperacion);
			if($resultadoRecuperacion){
				$idrecuperacion = $resultadoRecuperacion["idrecuperacion"];

				$asuntomail = "Recuperacion de contraseña Ticketsur";
				$cuerpomail = "La recuperacion de su contraseña fue ingresada con exito \n \n";
				$cuerpomail .= "Su id de recuperacion es ".$idrecuperacion." su codigo auto generado es ".$autogenerado." \n \n";
				
				$cuerpomail .= "Para recuperar su contraseña acceda al enlace : http://frontend.local/recuperarcontrasena.html?idrecuperacion=".$idrecuperacion."&autogenerado=".$autogenerado."&mail=".$mail;

				$resultadomail = $this->sendmail($mail,$asuntomail,$cuerpomail);
				//$resultadomail = true;
				if ($resultadomail) {
					echo (json_encode(["exito" => true, "info" => "Solicitud para recuperar la contraseña exitosa, recibira un mail con las indicaciones para recuperar la contraseña."]));
				}
				else{
					echo (json_encode(["exito" => true, "info" => "Solicitud para recuperar la contraseña exitosa, error al enviar el mail de recuperacion de contraseña."]));
				}


			}
			else{
				echo (json_encode(["exito" => false, "info" => "Error al ingresar la recuperacion de contraseña."]));	
			}		

		}
		else{
			echo (json_encode(["exito" => false, "info" => "El mail no existe en la base de datos o el usuario esta dado de baja."]));
		}	

	}

	public function generarCodigo(){
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 50; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function sendmail($destinatario,$asuntomail,$cuerpomail) {

        // Configure email library
		$this->load->library('mailer');
		$mail = $this->mailer->load();
    	$mail->isSMTP();
        $mail->Host = '192.168.21.62';
        $mail->Username = 'tickasur@tecnisur.local';
        $mail->Password = 'Administrator1';
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Port = 465;
        $mail->SMTPOptions = array(
		    'ssl' => [
		        'verify_peer' => false,
		        'verify_depth' => 3,
		        'allow_self_signed' => true,
		        'peer_name' => 'exchange.tecnisur.local'
		    ],
		);

        $mail->setFrom('tickasur@tecnisur.local', 'tickasur@tecnisur.local');
        $mail->addAddress($destinatario);
        $mail->Subject = $asuntomail;
        $mail->Body = $cuerpomail;
        // Send email

        if(!$mail->send()){
            return $mail->ErrorInfo;
        }else{
            return true;
        }
    }

    public function nuevaPassword() { //exito e info
    	$idrecuperacion = $this->input->post('idrecuperacion');
    	$autogenerado = $this->input->post('autogenerado');
    	$hash = $this->input->post('hash');

    	$recuperacion = $this->Client_model->recuperacionPorId($idrecuperacion);
    	$today = date("Y-m-d H:i:s");

    	if ($recuperacion) {
    		if ($recuperacion['validado'] == 0) {
	    		if ($recuperacion['expira'] > $today ) {
	    			if ($recuperacion['autogenerado'] == $autogenerado) {
	    				$idusuario = $recuperacion['idusuario'];
	    				if ($this->Client_model->bajaRecuperacion($idrecuperacion)) {
		    				if($this->Client_model->modificarCliente($idusuario,'hash',$hash)){
								echo json_encode(["exito" => true, "info" => "La contraseña fue actualizada con exito."]);
							}
							else{
								echo json_encode(["exito" => false, "info" => "Error al intentar actualizar la contraseña."]);	
							}
						}
						else{
							echo json_encode(["exito" => false, "info" => "Error al dar de baja el link de recuperacion de contraseña."]);
						}
	    			}
	    			else{
	    				echo json_encode(["exito" => false, "info" => "El codigo autogenerado no coincide."]);
	    			}
	    		}
	    		else{
	    			echo json_encode(["exito" => false, "info" => "El periodo para la recuparacion ya expiró."]);
	    		}
	    	}
	    	else{
	    		echo json_encode(["exito" => false, "info" => "El enlace de recuperacion ya fue utilizado."]);
	    	}
    	}
    	else{
    		echo json_encode(["exito" => false, "info" => "No se encuentra la recuperacion en la base de datos."]);
    	}
    }

    /*public function testmail() {

    	$this->load->library('mailer');
		$mail = $this->mailer->load();
    	$mail->isSMTP();
    	$mail->SMTPDebug = 4; // <- low level debug
        $mail->Host = '192.168.21.62';
        $mail->Username = 'tickasur@tecnisur.local';
        $mail->Password = 'Administrator1';
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Port = 465;
        $mail->SMTPOptions = array(
		    'ssl' => [
		        'verify_peer' => false,
		        'verify_depth' => 3,
		        'allow_self_signed' => true,
		        'peer_name' => 'exchange.tecnisur.local'
		    ],
		);

        $mail->setFrom('tickasur@tecnisur.local', 'tickasur@tecnisur.local');
        //$mail->addReplyTo('sorellano@tecnisur.local', 'sorellano');
        $mail->addAddress('adearmas@tecnisur.local');
        $mail->Subject = 'Test';
        //$mail->isHTML(true);
        $mail->Body = 'This is a plain-text message body';
        $mail->AddAttachment("/var/www/html/api/imagenes/2.png");
        // Send email
        if(!$mail->send()){
            echo 'Message could not be sent. \l\n';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }*/
}
