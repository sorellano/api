<?php
class Chat extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Chat_model');
        $this->load->helper('date');
    }

    public function empezarSala() {
    	$idusuario = $this->session->userdata('idusuario');
    	$expira = new DateTime();
		$expira->modify("+30 minutes");

		$valor = date_format($expira, 'Y-m-d H:i:s');
    	$sala = array(
    		"idcliente" => $idusuario,
    		"expira" => $valor
    	);
    	$errores = array();

    	if($this->Chat_model->online()){
    		$errores["ERROR"] = "Al parecer ya estas en linea.";
	        echo json_encode(array("exito" => false,"errores" => $errores));
    	}else{
    		$sala = $this->Chat_model->empezarSala($sala);
	    	if(!$sala){
	    		$errores["ERROR"] = "Error al intentar empezar la Sala";
	            echo json_encode(array("exito" => false,"errores" => $errores));
	    	}else{
	    		echo json_encode(array("exito" => true,"info" => "Espere hasta que un moderador atienda la sala."));
		    	$session = array(
			        'sala' => $sala
		       	);
	       		$this->session->set_userdata($session);
	        }
    	}
    }

    public function cerrarSala() {
    	$idusuario = $this->session->userdata('idusuario');
    	$idsala = $this->input->post("idsala");
    	//echo json_encode(array("idsala" => $idsala));
    	if(!$this->Chat_model->pertenezco($idsala)){
    		$errores["ERROR"] = "Al parecer no perteneces a la sala";
	        echo json_encode(array("exito" => false,"errores" => $errores));
    	}elseif($this->Chat_model->estaCerrada($idsala)){
    		$errores["ERROR"] = "Al parecer la sala ya esta cerrada";
	        echo json_encode(array("exito" => false,"errores" => $errores));
    	}elseif($this->Chat_model->expiro($idsala)){
    		$errores["ERROR"] = "Al parecer la sala expiro";
	        echo json_encode(array("exito" => false,"errores" => $errores));
    	}else{
    		$cerrarsala = $this->Chat_model->CerrarSala($idsala);
    		if($cerrarsala){
	    		$session = array(
			        'sala' => array()
		       	);
	       		$this->session->set_userdata($session);
	    		echo json_encode(array("exito" => true,"info" => "La sala se cerro con exito."));
	    	}else{
	    		$errores["ERROR"] = "Error al intentar cerrar la Sala";
	            echo json_encode(array("exito" => false,"errores" => $errores));
	    		
	    	}
    	}
    }

    public function online() {
    	//$idusuario = $this->session->userdata('idusuario');
    	if($this->Chat_model->online()){
    		echo json_encode(array("online" => true,"expira" => $this->Chat_model->obtenerExpira(),"idsala" => $this->Chat_model->obtenerIdsala()));
    	}else{
    		echo json_encode(array("online" => false));
    	}

    }

    public function obtenerMensajes() {
    	$idusuario = $this->session->userdata('idusuario');
    	$idsala = $this->input->post("idsala");
    	if(!$this->Chat_model->pertenezco($idsala)){
    		$errores["ERROR"] = "Al parecer no perteneces a la sala ".$idsala;
	        echo json_encode(array("exito" => false,"errores" => $errores));
    	}elseif($this->Chat_model->estaCerrada($idsala)){
    		$errores["ERROR"] = "Al parecer la sala fue cerrada ";
	        echo json_encode(array("exito" => false,"errores" => $errores));
    	}elseif($this->Chat_model->expiro($idsala)){
    		$errores["ERROR"] = "Al parecer la sala expiro";
	        echo json_encode(array("exito" => false,"errores" => $errores));
    	}else{
    		$mensajes = $this->Chat_model->obtenerMensajes($idsala);
    		if(isset($mensajes)){
    			echo json_encode(array("exito" => true,"mensajes" => $mensajes));
	    	}else{
	    		$errores["ERROR"] = "Error al consultar los mensajes";
	            echo json_encode(array("exito" => false,"errores" => $errores));
	    	}
    	}
    }

    public function enviarMensaje() { //Hay que hacer que enviar el mensaje actualice la hora de expira del chat
    	$idusuario = $this->session->userdata('idusuario');
    	$idsala = $this->input->post('idsala');
    	$mensaje = $this->input->post('mensaje');
    	//echo json_encode(array("idsala" => $idsala));
		if(!$this->Chat_model->pertenezco($idsala)){
    		$errores["ERROR"] = "Al parecer no perteneces a la sala";
	        echo json_encode(array("exito" => false,"errores" => $errores));
    	}elseif($this->Chat_model->estaCerrada($idsala)){
    		$errores["ERROR"] = "Al parecer la sala ya esta cerrada ".$idsala;
	        echo json_encode(array("exito" => false,"errores" => $errores));
    	}elseif($this->Chat_model->expiro($idsala)){
    		$errores["ERROR"] = "Al parecer la sala expiro";
	        echo json_encode(array("exito" => false,"errores" => $errores));
    	}else{
    		$mensaje = array(
    			"idsala" => $idsala,
    			"idautor" => $idusuario,
    			"tipoautor" => "CLIENTE",
    			"mensaje" => $mensaje
    		);
    		if($this->Chat_model->enviarMensaje($mensaje)){
    			echo json_encode(array("exito" => true));
	    	}else{
	    		$errores["ERROR"] = "Error al intentar enviar el mensaje";
	            echo json_encode(array("exito" => false,"errores" => $errores));
	    	}
    	}
    }
}