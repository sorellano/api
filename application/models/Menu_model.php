<?php
class Menu_model extends CI_Model {

	public function menuPorEvento($idevento) {
		$this->db->select('COMIDA.nombre as nombrecomida, MENU.precio as precio, MENU.idcomida as idcomida,COMIDA.imagen as imagen');
        $this->db->from('MENU');
        $this->db->join('COMIDA', 'MENU.idcomida = COMIDA.idcomida');
        $this->db->where ('MENU.idevento', $idevento);
        $this->db->where ('COMIDA.baja', 0);
        $this->db->where ('MENU.baja', 0);

        $consulta = $this->db->get();

        
        if ($consulta->num_rows() == 0)
            return false;

        $resultado = $consulta->result_array();
        return $resultado;
	}

    public function existe($idcomida,$idevento) {
        $this->db->select('*');
        $this->db->from('MENU');
        $this->db->where ('idcomida',$idcomida);
        $this->db->where ('idevento',$idevento);
        $this->db->where ('baja', 0);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        return true;
    }

    public function precio($idcomida,$idevento) {
        $this->db->select('precio');
        $this->db->from('MENU');
        $this->db->where ('idcomida',$idcomida);
        $this->db->where ('idevento',$idevento);
        $this->db->where ('baja', 0);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        $resultado = $consulta->row_array();
        return $resultado['precio'];
    }
}