<?php
class Metodopago_model extends CI_Model {

	public function listar() {
       $metodospago = array();

        $resultado = $this->db->query('SELECT * from METODOPAGO WHERE baja=0')->result_array();
        foreach($resultado as $metodopago)
            array_push($metodospago, $metodopago);

        return $metodospago;
    }
    
    public function existe($idmetodopago) {
        $this->db->select('*');
        $this->db->from('METODOPAGO');
        $this->db->where ('idmetodopago',$idmetodopago);
        $this->db->where ('baja', 0);

        $consulta = $this->db->get();

        
        if ($consulta->num_rows() == 0)
            return false;

        return true;
    }
}