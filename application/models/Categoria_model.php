<?php
class Categoria_model extends CI_Model {

	public function listar() {
       $categorias = array();

        $resultado = $this->db->query('SELECT * from CATEGORIA WHERE baja=0')->result_array();
        foreach($resultado as $categoria)
            array_push($categorias, $categoria);

        return $categorias;
    }
}