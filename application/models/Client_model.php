<?php
class Client_model extends CI_Model {

	public function validarInformacion($cliente) {
		$errores = array();

		/*---------------- Checks de email ----------------*/
		if ($this->existeMail($cliente['mail'])) {
						$errores['MAIL_INVALIDO'] = "El mail ya existe";
		}
		else{

			if ( !is_string($cliente['mail']) ) {
				$errores['MAIL_INVALIDO'] = "El email debe ser texto";
			} else {
				if ( !preg_match('/@/', $cliente['mail']) ) {
					$errores['MAIL_INVALIDO'] = "Los emails deben contener un arroba";
					$primer_caracter = substr($cliente['mail'], 0, 1);
					$ultimo_caracter = substr($cliente['mail'], -1);

					if ($primer_caracter == '@' || $ultimo_caracter == '@') {
						$errores['MAIL_INVALIDO'] = "El arroba debe estar en el medio";
					}		
				}
				$explode = explode( "." , $cliente['mail']);
		        if(!isset($explode[1])){
		             $errores['MAIL_INVALIDO'] = "Falta el sub-dominio";
		        }
			}
		}
		

		/*--------------- Checks de password ----------------
		if ( !is_string($cliente['password']) ) {
			$errores['PASSWORD_INVALIDO'] = "El password debe ser texto ".$cliente['password'];
		} else {
			$largoPassword = strlen($cliente['password']);
			if ($largoPassword < 8) {
				$errores['PASSWORD_INVALIDO'] = "El password debe tener un minimo de 8 caracteres y tiene ".$largoPassword;
			}
		}*/

		/*--------------- Checks de nombre ----------------*/
		if ( !is_string($cliente['nombre']) ) {
			$errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
		} else {
			$largoNombre = strlen($cliente['nombre']);
			if ($largoNombre > 20) {
				$errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 20 caracteres";
			}
		}

		/*--------------- Checks de apellido ----------------*/
		if ( !is_string($cliente['apellido']) ) {
			$errores['APELLIDO_INVALIDO'] = "El apellido debe ser texto";
		} else {
			$largoNombre = strlen($cliente['apellido']);
			if ($largoNombre > 20) {
				$errores['APELLIDO_INVALIDO'] = "El apellido debe ser menor o igual a 20 caracteres";
			}
		}

		/*--------------- Checks de telefono ----------------*/
		if ( !ctype_digit ( $cliente['telefono'] )) {
			$errores['TELEFONO_INVALIDO'] = "El telefono ".$cliente['telefono']." contiene caracteres no numericos";
		}
		else{
			if ( !is_string($cliente['telefono']) ) {
				$errores['TELEFONO_INVALIDO'] = "El telefono tiene que ser un texto";
			} else {
				$largoTelefono = strlen($cliente['telefono']);
				if ( $largoTelefono < 8 || $largoTelefono > 9 ) {
					$errores['TELEFONO_INVALIDO'] = "Largo del telefono incorrecto, el largo debe ser entre 8 y 9 caracteres y es de ".$largoTelefono." caracteres";
				} else {
					//Si el largo es 8 es un telefono de linea
					if ($largoTelefono == 8) {
						$prefijo = substr($cliente['telefono'], 0, 1);
						if ($prefijo !== '2' || $prefijo !== '4') {
							$errores['TELEFONO_INVALIDO'] = "Los telefonos de linea tienen que empezar con 2 o 4";
						}
					}
					//Si el largo es 9 es un celular
					if ($largoTelefono == 9) {
						$prefijo = substr($cliente['telefono'], 0, 2);
						if ($prefijo !== '09') {
							$errores['TELEFONO_INVALIDO'] = "Los celulares tienen que empezar con 09";
						}
					}
				}
			}
		}

		/*---------------- Checks de ciudad ----------------------*/
		if ( !is_string($cliente['ciudad']) ) {
			$errores['CIUDAD_INVALIDA'] = "El ciudad debe ser texto";
		} else {
			$largoNombre = strlen($cliente['ciudad']);
			if ($largoNombre > 15) {
				$errores['CIUDAD_INVALIDA'] = "El ciudad debe ser menor o igual a 20 caracteres";
			}
		}

		/*---------------- Checks de calle ----------------------*/
		if ( !is_string($cliente['calle']) ) {
			$errores['CALLE_INVALIDO'] = "El calle debe ser texto";
		} else {
			$largoNombre = strlen($cliente['calle']);
			if ($largoNombre > 20) {
				$errores['CALLE_INVALIDO'] = "El calle debe ser menor o igual a 20 caracteres";
			}
		}

		/*---------------- Checks de barrio ----------------------*/
		if ( !is_string($cliente['barrio']) ) {
			$errores['BARRIO_INVALIDO'] = "El barrio debe ser texto";
		} else {
			$largoNombre = strlen($cliente['barrio']);
			if ($largoNombre > 15) {
				$errores['BARRIO_INVALIDO'] = "El barrio debe ser menor o igual a 20 caracteres";
			}
		}

		/*---------------- Checks de CI ----------------------*/
		if ( !is_string($cliente['ci']) ) {
			$errores['CI_INVALIDA'] = "La cedula debe que ser un texto";
		} else {
			$largo_cedula = strlen($cliente['ci']);
			if ($largo_cedula < 7 || $largo_cedula > 8) {
				$errores['CI_INVALIDA'] = "Largo de la cedula incorrecto";
			} else {
				if ( !ctype_digit($cliente['ci'])) {
					$errores['CI_INVALIDA'] = "La cedula contiene caracteres invalidos";
				}
				else{
					if ($this->existeCi($cliente['ci'])) {
						$errores['CI_INVALIDA'] = "La cedula ya existe";
					}
				}

				//TODO: Verificar usando el digito verificador
			}
		}

		//TODO: Checks de saldo opcional
		// /*---------------- Checks de saldo ----------------------*/
		// // is_numeric() tambien es true si es String con numeros, por eso hay que revisar que no sea un String manualmente
		// if ( !is_numeric($cliente['saldo']) || is_string($cliente['saldo']) ) {
		// 	$errores['SALDO_INVALIDO'] = "El saldo debe ser un numero";
		// } else {
		// 	if ($cliente['saldo'] < 0) {
		// 		$errores['SALDO_INVALIDO'] = "El saldo no puede ser negativo";
		// 	}
		// }

		return $errores;
	}

	public function validarHash($hash){
		$errores = array();
		/*--------------- Checks de hash ----------------*/
		if ( !is_string($hash) ) {
			$errores['HASH_INVALIDO'] = "El hash debe ser texto";
		} else {
			$largoHash = strlen($hash);
			if ($largoHash != 64) {
				$errores['HASH_INVALIDO'] = "El hash debe tener 64 caracteres (sha256)";
			} else {
				if ( preg_match('([^0-9a-fA-F])', $hash) ) {
					$errores['HASH_INVALIDO'] = "El hash debe tener solo caracteres hexadecimales";
				}
			}
		}
		return $errores;
	}

	public function validarNombre($nombre){
		$errores = array();
		/*--------------- Checks de nombre ----------------*/
		if ( !is_string($nombre) ) {
			$errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
		} else {
			$largoNombre = strlen($nombre);
			if ($largoNombre > 20) {
				$errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 20 caracteres";
			}
		}
		return $errores;
	}

	public function validarApellido($apellido){
		$errores = array();
		/*--------------- Checks de apellido ----------------*/
		if ( !is_string($apellido) ) {
			$errores['APELLIDO_INVALIDO'] = "El apellido debe ser texto";
		} else {
			$largoNombre = strlen($apellido);
			if ($largoNombre > 20) {
				$errores['APELLIDO_INVALIDO'] = "El apellido debe ser menor o igual a 20 caracteres";
			}
		}
		return $errores;
	}

	public function validarTelefono($telefono){
		$errores = array();
		/*--------------- Checks de telefono ----------------*/
		if ( !ctype_digit ( $telefono )) {
			$errores['TELEFONO_INVALIDO'] = "El telefono ".$telefono." contiene caracteres no numericos";
		}
		else{
			if ( !is_string($telefono) ) {
				$errores['TELEFONO_INVALIDO'] = "El telefono tiene que ser un texto";
			} else {
				$largoTelefono = strlen($telefono);
				if ( $largoTelefono < 8 || $largoTelefono > 9 ) {
					$errores['TELEFONO_INVALIDO'] = "Largo del telefono incorrecto, el largo debe ser entre 8 y 9 caracteres y es de ".$largoTelefono." caracteres";
				} else {
					//Si el largo es 8 es un telefono de linea
					if ($largoTelefono == 8) {
						$prefijo = substr($telefono, 0, 1);
						if ($prefijo !== '2' || $prefijo !== '4') {
							$errores['TELEFONO_INVALIDO'] = "Los telefonos de linea tienen que empezar con 2 o 4";
						}
					}
					//Si el largo es 9 es un celular
					if ($largoTelefono == 9) {
						$prefijo = substr($telefono, 0, 2);
						if ($prefijo !== '09') {
							$errores['TELEFONO_INVALIDO'] = "Los celulares tienen que empezar con 09";
						}
					}
				}
			}
		}
		return $errores;
	}

	public function validarCiudad($ciudad){
		$errores = array();
		/*---------------- Checks de ciudad ----------------------*/
		if ( !is_string($ciudad) ) {
			$errores['CIUDAD_INVALIDA'] = "El ciudad debe ser texto";
		} else {
			$largoNombre = strlen($ciudad);
			if ($largoNombre > 15) {
				$errores['CIUDAD_INVALIDA'] = "El ciudad debe ser menor o igual a 20 caracteres";
			}
		}
		return $errores;
	}

	public function validarCalle($calle){
		$errores = array();
		/*---------------- Checks de calle ----------------------*/
		if ( !is_string($calle) ) {
			$errores['CALLE_INVALIDO'] = "El calle debe ser texto";
		} else {
			$largoNombre = strlen($calle);
			if ($largoNombre > 20) {
				$errores['CALLE_INVALIDO'] = "El calle debe ser menor o igual a 20 caracteres";
			}
		}
		return $errores;
	}

	public function validarBarrio($barrio){
		$errores = array();
		/*---------------- Checks de barrio ----------------------*/
		if ( !is_string($barrio) ) {
			$errores['BARRIO_INVALIDO'] = "El barrio debe ser texto";
		} else {
			$largoNombre = strlen($barrio);
			if ($largoNombre > 15) {
				$errores['BARRIO_INVALIDO'] = "El barrio debe ser menor o igual a 20 caracteres";
			}
		}
		return $errores;
	}

	public function validarSaldo($saldo){
		$errores = array();

		 /*---------------- Checks de saldo ----------------------*/
		//  is_numeric() tambien es true si es String con numeros, por eso hay que revisar que no sea un String manualmente
		if (!ctype_digit ( $saldo )) {
		 	$errores['SALDO_INVALIDO'] = "El saldo debe ser un numero";
		}
		else {
		 	if ($saldo < 0) {
		 		$errores['SALDO_INVALIDO'] = "El saldo no puede ser negativo";
		 	}
		}

		return $errores;
	}

	public function autenticarPorHash($hash) {
		$resultado = $this->db->query('SELECT * FROM CLIENTE WHERE hash=? and baja=0', $hash);

		if ($resultado->num_rows() == 0)
			return false;

		// row_array devuelve el la primera fila como un array
		$cliente = $resultado->row_array();

		if ($cliente['hash'] != $hash)
			return false;

		return $cliente;
	}

	public function perfilPorId($idusuario) {
		$resultado = $this->db->query('SELECT * FROM CLIENTE WHERE idusuario=? and baja=0', $idusuario);

		if ($resultado->num_rows() == 0)
			return false;

		$cliente = $resultado->row_array();

		return $cliente;
	}

	public function modificarCliente($idusuario, $clave, $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('idusuario', $idusuario);

        if ( !$this->db->update ('CLIENTE') )
            return false;

        return true;
    }

	public function generarHash($mail, $password) {
		return hash('sha256', $mail.":".$password);
	}

	public function darDeBaja($idusuario) {
		$this->db->set   ('baja', true);
		$this->db->where ('idusuario', $idusuario);
		if ( !$this->db->update ('CLIENTE') )
			return false;

		return true;
	}

	public function insertar($cliente) {
		if ( $this->db->query('SELECT * FROM CLIENTE WHERE mail=?', $cliente['mail'])->num_rows() > 0 )
			return false;

		if ( !$this->db->insert("CLIENTE", $cliente) )
			return false;

		return true;
	}

	public function existeMail($mail) {
        $resultado = $this->db->query('SELECT * FROM CLIENTE WHERE mail=?', $mail);

        if ($resultado->num_rows() == 0)
			return false;

        return true;
    }

    public function existeCi($ci) {
        $resultado = $this->db->query('SELECT * FROM CLIENTE WHERE ci=?', $ci);

        if ($resultado->num_rows() == 0)
			return false;

        return true;
    }

    public function existeIdUsuario($idusuario) {
        $resultado = $this->db->query('SELECT * FROM CLIENTE WHERE idusuario=?', $idusuario);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function dadoDeBaja($idusuario) {
        $resultado = $this->db->query('SELECT * FROM CLIENTE WHERE idusuario=? AND BAJA=1', $idusuario);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function solicitarSaldo($solicitar) {

		if ( !$this->db->insert("SALDO", $solicitar) )
			return false;

		return true;
	}

	public function saldo() {

		$this->db->select('saldo');
        $this->db->from('CLIENTE');
        $this->db->where ('idusuario', $this->session->userdata('idusuario'));
        $this->db->where ('baja', 0);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        $resultado = $consulta->row_array();
		return $resultado['saldo'];
	}

	public function cambiarSaldo($idusuario,$saldo) {
		$this->db->set   ('saldo', $saldo);
		$this->db->where ('idusuario', $idusuario);
		if ( !$this->db->update ('CLIENTE') )
			return false;

		return true;
	}

	public function mail($idusuario) {
        $this->db->select('mail');
        $this->db->from('CLIENTE');
        $this->db->where ('idusuario',$idusuario);
        $this->db->where ('baja', 0);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        $resultado = $consulta->row_array();
        return $resultado['mail'];
    }

    public function idusuarioPorMail($mail) {
        $this->db->select('idusuario');
        $this->db->from('CLIENTE');
        $this->db->where ('mail',$mail);
        $this->db->where ('baja', 0);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        $resultado = $consulta->row_array();
        return $resultado['idusuario'];
    }

    public function insertarRecuperacion($recuperacion) {

		if ( !$this->db->insert("RECUPERACIONES", $recuperacion) )
			return false;

		$id = $this->db->insert_id();
		$q = $this->db->get_where('RECUPERACIONES', array('idrecuperacion' => $id));
		return $q->row_array();
	}

	public function recuperacionPorId($idrecuperacion) {
        $this->db->select('*');
        $this->db->from('RECUPERACIONES');
        $this->db->where ('idrecuperacion',$idrecuperacion);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        $resultado = $consulta->row_array();
        return $resultado;
    }

    public function bajaRecuperacion($idrecuperacion) {
        $this->db->set   ('validado', 1);
        $this->db->where ('idrecuperacion', $idrecuperacion);

        if ( !$this->db->update ('RECUPERACIONES') )
            return false;

        return true;
    }
}
