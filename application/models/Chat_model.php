<?php
class Chat_model extends CI_Model {

	public function empezarSala($sala){ //Insertar Sala
		if ( !$this->db->insert("SALA", $sala) )
            return false;

        $id = $this->db->insert_id();
		$q = $this->db->get_where('SALA', array('idsala' => $id));

        $sala = $q->row_array();
        $mensaje = array(
                "idsala" => $sala['idsala'],
                "idautor" => 0,
                "tipoautor" => "INFO",
                "mensaje" => "Estamos buscando a alguien para atenderte danos un momento"
        );
        $this->enviarMensaje($mensaje);

		return $sala;
	}

	public function CerrarSala($idsala){
		$this->db->set   ('cerrada', true);
        $this->db->where ('idsala', $idsala);

        if ( !$this->db->update ('SALA') )
            return false;

        return true;
	}

    public function actualizarExpira($idsala){
        $expira = new DateTime();
        $expira->modify("+30 minutes");
        $valor = date_format($expira, 'Y-m-d H:i:s');

        $this->db->set   ('expira', $valor);
        $this->db->where ('idsala', $idsala);

        if ( !$this->db->update ('SALA') )
            return false;

        return true;
    }

    public function obtenerIdsala(){
        $idusuario = $this->session->userdata('idusuario');
        $ahora = date('Y-m-d H:i:s');
        $this->db->select('idsala');
        $this->db->from('SALA');
        $this->db->where('cerrada', 0);
        $this->db->where('idcliente', $idusuario);
        $this->db->where('expira >', $ahora);
        $query = $this->db->get();
        $row = $query->row();
        if (isset($row)){
            return $row->idsala;
        }else{
            return false;
        }
    }

	public function online(){
		$idusuario = $this->session->userdata('idusuario');
    	$ahora = date('Y-m-d H:i:s');
    	$this->db->select('idsala');
    	$this->db->from('SALA');
    	$this->db->where('cerrada', 0);
    	$this->db->where('idcliente', $idusuario);
    	$this->db->where('expira >', $ahora);
    	$query = $this->db->get();
    	if ($query->num_rows() == 0)
    		return false;            
        else
        	return true;
	}

	public function obtenerExpira(){
		$idusuario = $this->session->userdata('idusuario');
    	$ahora = date('Y-m-d H:i:s');
    	$this->db->select('idsala,expira');
    	$this->db->from('SALA');
    	$this->db->where('cerrada', 0);
    	$this->db->where('idcliente', $idusuario);
    	$this->db->where('expira >', $ahora);
    	$query = $this->db->get();
    	$row = $query->row();

		if (isset($row)){
		    return $row->expira;
		}else{
			return false;
		}
	}

	public function obtenerMensajes($idsala){
    	$ahora = date('Y-m-d H:i:s');
    	$this->db->select('tipoautor,mensaje');
    	$this->db->from('MENSAJE');
    	$this->db->join('SALA', 'MENSAJE.idsala = SALA.idsala');
    	$this->db->where('cerrada', 0);
    	$this->db->where('expira >', $ahora);
        $this->db->where('MENSAJE.idsala', $idsala);
        $this->db->order_by('idmensaje', 'desc');
    	$this->db->limit(10);
    	$query = $this->db->get();
    	return $query->result_array();
	}
	public function enviarMensaje($mensaje){
		if ( !$this->db->insert("MENSAJE", $mensaje) )
            return false;

        $idsala = $mensaje['idsala'];
        $this->actualizarExpira($idsala);
        return true;
	}
	public function pertenezco($idsala){
		$idusuario = $this->session->userdata('idusuario');
        //$idsala = $this->session->userdata('sala["idsala"]');
    	$this->db->select('idsala');
    	$this->db->from('SALA');
    	$this->db->where ('idsala', $idsala);
    	$this->db->where('idcliente', $idusuario);
    	$query = $this->db->get();

    	if ($query->num_rows() == 0)
    		return false;            
        else
        	return true;
    }

    public function estaCerrada($idsala){
    	$this->db->select('idsala');
    	$this->db->from('SALA');
    	$this->db->where ('idsala', $idsala);
    	$this->db->where('cerrada', 1);
    	$query = $this->db->get();

    	if ($query->num_rows() == 0)
    		return false;            
        else
        	return true;
    }

    public function expiro($idsala){
        $ahora = date('Y-m-d H:i:s');
    	$this->db->select('idsala');
    	$this->db->from('SALA');
    	$this->db->where ('idsala', $idsala);
    	$this->db->where('expira <', $ahora);
    	$query = $this->db->get();

    	if ($query->num_rows() == 0){
    		return false;            
        }
        else{
            $row = $query->row();
            if (isset($row)){
                return $this->CerrarSala($row->idsala);
            }else{
                return false;
            }
        }
    }
}