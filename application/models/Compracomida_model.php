<?php
class Compracomida_model extends CI_Model {

	public function insertar($comida) {
		if ( !$this->db->insert("COMPRACOMIDA", $comida) )
			return false;

		$id = $this->db->insert_id();
		$q = $this->db->get_where('COMPRACOMIDA', array('identificador' => $id));
		return $q->row_array();
	}

}