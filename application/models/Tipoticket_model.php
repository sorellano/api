<?php
class Tipoticket_model extends CI_Model {

	public function tipoticketsPorEvento($idevento) {
		$this->load->model('Ticket_model');
        $this->load->model('Evento_model');
        $fechas = $this->Evento_model->fechasPorId($idevento);

        if($fechas){

            $resultado = array();
            foreach ($fechas as $fecha){                

                $this->db->select('idtipoticket, nombre, cantidad, numerado, precio, referencia');
                $this->db->from('TIPOTICKET');
                $this->db->where ('baja', 0);
                $this->db->where ('fecha', $fecha['fecha']);
                $this->db->where ('idevento', $idevento);
                $consulta = $this->db->get();
                $tickets = $consulta->result_array();

                $tickets2 = array();
                foreach ($tickets as $tickets => $ticket) {
                    $idtipoticket = $ticket['idtipoticket'];
                    $numerado = $ticket['numerado'];
                    $vendidas = $this->Ticket_model->ticketsVendidos($idtipoticket,$numerado);
                    array_push($tickets2, array_merge(['vendidas' => $vendidas], $ticket));
                }

                if($consulta->num_rows() != 0){
                    
                    array_push($resultado, array_merge(['fecha' => $fecha['fecha']], $tickets2)); //Tickets 2 contiene la informacion de los id de tickets y la informacion de las ventanas para comprobar el stock.
                }

            }
            if($resultado){
                return $resultado;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

	}

    public function fecha($idtipoticket) {
        $this->db->select('fecha');
        $this->db->from('TIPOTICKET');
        $this->db->where ('idtipoticket',$idtipoticket);
        $this->db->where ('baja', 0);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        return $consulta->row_array();

    }

    public function existe($idtipoticket,$idevento) {
        $this->db->select('*');
        $this->db->from('TIPOTICKET');
        $this->db->where ('idtipoticket',$idtipoticket);
        $this->db->where ('idevento',$idevento);
        $this->db->where ('baja', 0);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        return true;
    }

    public function precio($idtipoticket) {
        $this->db->select('precio');
        $this->db->from('TIPOTICKET');
        $this->db->where ('idtipoticket',$idtipoticket);
        $this->db->where ('baja', 0);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        return $consulta->row_array();
    }

    public function cantidad($idtipoticket) {
        $this->db->select('cantidad');
        $this->db->from('TIPOTICKET');
        $this->db->where ('idtipoticket',$idtipoticket);
        $this->db->where ('baja', 0);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        return $consulta->row_array();
    }

    public function stock($idtipoticket) {

        $this->db->select('idtipoticket, cantidad, numerado');
        $this->db->from('TIPOTICKET');
        $this->db->where ('baja', 0);
        $this->db->where ('idtipoticket', $idtipoticket);
        $consulta = $this->db->get();
        $tipoticket = $consulta->row_array();

        if ($consulta->num_rows() == 0)
            return 0;

        $vendidas = $this->Ticket_model->ticketsVendidos($tipoticket['idtipoticket'],$tipoticket['numerado']);

        if (isset($vendidas['vacio'])) {
            $cantidadvendidas = 0;
        }else{
            if(!$tipoticket['numerado']){
                $cantidadvendidas = $vendidas['cantidad'];
            }
            else{
                $cantidadvendidas = count($vendidas);
            }
        }

        $stock =  $tipoticket['cantidad'] - $cantidadvendidas;

        if ($stock <= 0) {
            return 0;
        }else{
            return floor($stock);
        }
    }
}