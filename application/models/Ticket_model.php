<?php
class Ticket_model extends CI_Model {

	public function ultimoNumero($idtipoticket) {

		$this->db->select_max('numero');
        $this->db->from('TICKET');
        $this->db->where ('idtipoticket',$idtipoticket);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return 0;

        $resultado = $consulta->row_array();

        if($resultado['numero'] == null){
        	return 0;
        }
        else{
        	return $resultado['numero'];
        }        
	}

	public function insertar($ticket) {
		if ( !$this->db->insert("TICKET", $ticket) )
			return false;

		$id = $this->db->insert_id();
		$q = $this->db->get_where('TICKET', array('idticket' => $id));
		return $q->row_array();
	}

    public function ticketsVendidos($idtipoticket,$numerado) {
    	$this->db->select('idtipoticket');
        $this->db->select('numero');
        $this->db->from('TICKET');
        $this->db->where ('idtipoticket', $idtipoticket);
		$consulta = $this->db->get();

        if ($consulta->num_rows() == 0)
	            return array('idtipoticket' => $idtipoticket, 'vacio' => true);

        if(!$numerado){
			$count = $consulta->num_rows();
			return array('idtipoticket' => $idtipoticket, 'cantidad' => $count);
        }
        else{
	        $resultado = $consulta->result_array();
	        return $resultado;
        }
    }


}