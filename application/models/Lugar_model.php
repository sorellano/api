<?php
class Lugar_model extends CI_Model {

	public function listar() {
       $lugares = array();

        $resultado = $this->db->query('SELECT * from LUGAR WHERE baja=0')->result_array();
        foreach($resultado as $lugar)
            array_push($lugares, $lugar);

        return $lugares;
    }
}