<?php
class Evento_model extends CI_Model {

	public function buscar($buscar,$inicio = FALSE,$idlugar = FALSE, $idcategoria = FALSE){

        $this->db->select('EVENTO.nombre as nombreevento, EVENTO.idevento, EVENTO.imagen, EVENTO.descripcion, LUGAR.nombre as nombrelugar, CATEGORIA.nombre as nombrecategoria');
        $this->db->from('EVENTO');
        $this->db->join('LUGAR', 'EVENTO.idlugar = LUGAR.idlugar');
        $this->db->join('CATEGORIA', 'EVENTO.idcategoria = CATEGORIA.idcategoria');
        $this->db->like("EVENTO.nombre",$buscar);
        $this->db->where ('EVENTO.baja', 0);
        if ($inicio !== FALSE) {
            $this->db->limit(20,$inicio);
        }
        if ($idcategoria !== FALSE) {
            $this->db->where ('CATEGORIA.idcategoria', $idcategoria);
            $this->db->where ('CATEGORIA.baja', 0);
        }
        if ($idlugar !== FALSE) {
            $this->db->where ('LUGAR.idlugar', $idlugar);
            $this->db->where ('LUGAR.baja', 0);
        }

        $consulta = $this->db->get();

        $resultado = $consulta->result_array();
        
        $return = array();
        foreach ($resultado as $fila)
            {
                $fechas = [ 'fechas' => $this->fechasPorId($fila['idevento']) ];

                if($fechas['fechas']){
                	$fila = array_merge($fila, $fechas);
                	array_push($return, $fila);
                }
                
            } 
        return $return;
    }

    public function carousel(){

        $this->db->select('EVENTO.nombre as nombreevento, EVENTO.idevento, EVENTO.imagen, EVENTO.descripcion, LUGAR.nombre as nombrelugar, EVENTO.prioridad as prioridadevento');
        $this->db->from('EVENTO');
        $this->db->join('LUGAR', 'EVENTO.idlugar = LUGAR.idlugar');
        $this->db->where ('EVENTO.baja', 0);
        $this->db->order_by("EVENTO.prioridad asc");
        $this->db->limit(16);
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        $return = array();
        foreach ($resultado as $fila)
            {
                $fechas = [ 'fechas' => $this->fechasPorId($fila['idevento']) ];

                if($fechas['fechas']){
                    $fila = array_merge($fila, $fechas);
                    array_push($return, $fila);
                }   
            } 
        return $return;
    }

    public function proximoseventos(){
        $this->db->select('EVENTO.nombre as nombreevento, EVENTO.idevento, EVENTO.imagen, EVENTO.descripcion, LUGAR.nombre as nombrelugar, EN.fecha');
        $this->db->from('EVENTO');
        $this->db->group_by('EN.idevento');
        $this->db->join('LUGAR', 'EVENTO.idlugar = LUGAR.idlugar');
        $this->db->join('EN', 'EVENTO.idevento = EN.idevento');
        $this->db->where ('EVENTO.baja', 0);
        $this->db->where('EN.fecha >=', date("Y-m-d h:i:sa"));
        $this->db->order_by('EN.fecha asc');
        $this->db->limit(6);
        $consulta = $this->db->get();
        $resultado = $consulta->result_array();
        return $resultado;
    }

    public function fechasPorId($idevento) {
        $fechas = array();

        $query = $this->db->query('SELECT * from EN WHERE idevento=? AND fecha >=?',array($idevento, date("Y-m-d h:i:sa")) );
        $resultado = $query->result_array();
        foreach($resultado as $fecha){
            array_push($fechas, $fecha);
        }

        if ($query->num_rows() == 0)
            return false;
        else
        return $fechas;
    }


    //Completar funcion de detalle
    public function detalle($idevento) {
    	$this->db->select('EVENTO.*');
    	$this->db->select('CATEGORIA.nombre as nombrecategoria');
        $this->db->select('LUGAR.nombre as nombrelugar');
         $this->db->select('LUGAR.plano as plano');
        $this->db->select('LUGAR.plazas as capacidadlugar');
        $this->db->from('EVENTO');
        $this->db->join('LUGAR', 'EVENTO.idlugar = LUGAR.idlugar');
        $this->db->join('CATEGORIA', 'EVENTO.idcategoria = CATEGORIA.idcategoria');
        $this->db->where ('EVENTO.idevento', $idevento);
        $this->db->where ('EVENTO.baja', 0);

        $consulta = $this->db->get();

        
        if ($consulta->num_rows() == 0)
            return false;

        $resultado = $consulta->row_array();
        return $resultado;
    }

    public function valido($idevento) {
        /*$query = $this->db->query('SELECT * from EN WHERE idevento=? AND fecha >=?',array($idevento, date("Y-m-d h:i:sa")) );*/
        $this->db->select('*');
        $this->db->from('EVENTO');
        $this->db->join('EN', 'EVENTO.idevento = EN.idevento');
        $this->db->where ('EVENTO.idevento', $idevento);
        $this->db->where ('EVENTO.baja', 0);
        $this->db->where ('EN.fecha >=', date("Y-m-d h:i:sa"));

        $query = $this->db->get();
        if ($query->num_rows() == 0)
            return false;
        else
            return true;
    }
}