<?php
class Compra_model extends CI_Model {

	public function insertar($compra) {

		if ( !$this->db->insert("COMPRA", $compra) )
			return false;

		
		$id = $this->db->insert_id();
		$q = $this->db->get_where('COMPRA', array('idcompra' => $id));
		return $q->row_array();
	}
}