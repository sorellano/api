<?php
class Paquete_model extends CI_Model {

	public function paquetesPorEvento($idevento) {

		$this->db->select('*');
		$this->db->from('PAQUETE');
		$this->db->where ('PAQUETE.idevento', $idevento);
		$this->db->where ('PAQUETE.baja', 0);
		$consulta = $this->db->get();
		$paquetes = $consulta->result_array();

		$resultado = array();
		foreach ($paquetes as $paquetes => $paquete) {

			$metodosdepago = $this->metodosdepago($paquete['idpaquete'],$paquete['idevento']);
			$comidas       = $this->comidas($paquete['idpaquete'],$paquete['idevento']);
			$tipotickets   = $this->tipotickets($paquete['idpaquete'],$paquete['idevento']);
			$precio        = $this->calcularPrecio($comidas,$tipotickets);
			
			if($metodosdepago && $precio>0 ){
				$paquete = array_merge(['metodosdepago' => $metodosdepago], $paquete);
				$paquete = array_merge(['comidas' => $comidas], $paquete);
				$paquete = array_merge(['tipotickets' => $tipotickets], $paquete);
				$paquete = array_merge(['precio' => $precio], $paquete);
				array_push($resultado, $paquete);
			}
		}

		return $resultado;
	}

	public function metodosdepago($idpaquete,$idevento) {
		$this->db->select('*');
		$this->db->from('PAQUETEMETODOPAGO');
		$this->db->from('METODOPAGO');
		$this->db->where ('PAQUETEMETODOPAGO.idpaquete', $idpaquete);
		$this->db->where ('PAQUETEMETODOPAGO.idmetodopago = METODOPAGO.idmetodopago');
		$this->db->where ('METODOPAGO.baja', 0);
		$consulta = $this->db->get();
		$metodosdepago = $consulta->result_array();
		return $metodosdepago;
	}

	public function comidas($idpaquete,$idevento) {
		$this->db->select('*');

		$this->db->from('PAQUETECOMIDA');
		$this->db->from('COMIDA');
		$this->db->from('MENU');
		$this->db->where ('PAQUETECOMIDA.idpaquete', $idpaquete);
		$this->db->where ('PAQUETECOMIDA.idcomida = COMIDA.idcomida');
		$this->db->where ('MENU.idcomida = COMIDA.idcomida');
		$this->db->where ('idevento', $idevento);
		$this->db->where ('COMIDA.baja', 0);
		$this->db->where ('MENU.baja', 0);
		$consulta = $this->db->get();
		$comidas = $consulta->result_array();
		return $comidas;
	}

	public function tipotickets($idpaquete,$idevento) {
		$this->db->select('*');
		$this->db->select('PAQUETETIPOTICKET.cantidad as cantidad');
		$this->db->select('TIPOTICKET.cantidad as cantidadtotal');
		$this->db->from('PAQUETETIPOTICKET');
		$this->db->from('TIPOTICKET');
		$this->db->where ('PAQUETETIPOTICKET.idpaquete', $idpaquete);
		$this->db->where('TIPOTICKET.idevento', $idevento);
		$this->db->where ('PAQUETETIPOTICKET.idtipoticket = TIPOTICKET.idtipoticket');
		$this->db->where ('TIPOTICKET.baja', 0);
		$consulta = $this->db->get();
		$tipotickets = $consulta->result_array();

		$tickets2 = array();
		foreach ($tipotickets as $tipotickets => $tipoticket) {
			//print_r($valor) ;


			$idtipoticket = $tipoticket['idtipoticket'];
            $numerado = $tipoticket['numerado'];
            $vendidas = $this->Ticket_model->ticketsVendidos($idtipoticket,$numerado);
            array_push($tickets2, array_merge(['vendidas' => $vendidas], $tipoticket));

		}
		return $tickets2;
	}

	public function calcularPrecio($comidas,$tipotickets) {
		$precio = 0;
		foreach ($comidas as $comidas => $comida) {
			$suma = $comida['precio']*$comida['cantidad'];
			$descuento = $suma/$comida['descuento'];
			$precio += $suma-$descuento;
		}
		foreach ($tipotickets as $tipotickets => $tipoticket) {
			$suma = $tipoticket['precio']*$tipoticket['cantidad'];
			$descuento = $suma/$tipoticket['descuento'];
		
			$precio += $suma-$descuento;
		}
		return $precio;
	}


	public function existe($idpaquete) {
        $this->db->select('*');
        $this->db->from('PAQUETE');
        $this->db->where ('idpaquete',$idpaquete);
        $this->db->where ('baja', 0);
        $consulta = $this->db->get();        
        if ($consulta->num_rows() == 0)
            return false;

        return true;
    }

    public function buscarPorId($idpaquete) {
        $resultado = $this->db->query('SELECT * FROM PAQUETE WHERE idpaquete=?', $idpaquete);

        if ($resultado->num_rows() == 0)
            return false;

        $paquete = $resultado->row_array();

        return $paquete;
    }

    public function stock($idpaquete) {


    	$stock = 0;
    	$resultado = $this->db->query('SELECT * FROM PAQUETE WHERE idpaquete=? and baja = 0', $idpaquete);

        if ($resultado->num_rows() == 0)
            return 0;

        $paquete = $resultado->row_array();
        $idevento = $paquete['idevento'];
        $tipotickets = $this->tipotickets($idpaquete,$idevento);

        $this->load->model('Tipoticket_model');

        $maximas = []; //la cantidad maxima que se puede pedir de dicho paquete en base al stock de cada tipo de ticket
        foreach ($tipotickets as $index => $tipoticket) {

        	$total = $tipoticket['cantidadtotal'];
        	$unidadesporpaquete = $tipoticket['cantidad'];
        	$vendidas = $tipoticket['vendidas'];
        	if (isset($vendidas['vacio']) ) {
        		$cantidadvendidas = 0;
        	}else{
        		if ($tipoticket['numerado']) {
        			$cantidadvendidas = count($vendidas);
        		}else{
        			$cantidadvendidas = $vendidas['cantidad'];
        		}
        	}
        	$cantidadmaxima = ($total-$cantidadvendidas)/$unidadesporpaquete;
        	array_push($maximas, floor($cantidadmaxima) );
        }
        if(empty($maximas)){
        	return 10;
        }else{ return min($maximas); }
    	return min($maximas);
    }


    public function precioPorId($idpaquete) {
		$precio = 0; 

		$paquete       = $this->buscarPorId($idpaquete);
		$comidas       = $this->comidas($paquete['idpaquete'],$paquete['idevento']);
		$tipotickets   = $this->tipotickets($paquete['idpaquete'],$paquete['idevento']);

		//$comidas,$tipotickets
		foreach ($comidas as $comidas => $comida) {
			$suma = $comida['precio']*$comida['cantidad'];
			$descuento = $suma/$comida['descuento'];
			$precio += $suma-$descuento;
		}
		foreach ($tipotickets as $tipotickets => $tipoticket) {
			$suma = $tipoticket['precio']*$tipoticket['cantidad'];
			$descuento = $suma/$tipoticket['descuento'];
		
			$precio += $suma-$descuento;
		}

		return $precio;
	}

	public function insertarcompra($paquete) {
		if ( !$this->db->insert("COMPRAPAQUETE", $paquete) )
			return false;

		return true;
	}

	public function ticketsdelpaquete($idpaquete) {
		$this->db->select('*');
		$this->db->from('PAQUETETIPOTICKET');
		$this->db->where ('idpaquete', $idpaquete);
		$consulta = $this->db->get();
		$tipotickets = $consulta->result_array();

		return $tipotickets;
	}
}